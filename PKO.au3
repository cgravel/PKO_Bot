#Include <Misc.au3>
AutoItSetOption("pixelcoordmode", 2)
AutoItSetOption("mousecoordmode", 2)
WinActivate("Pirate")
WinWaitActive("Pirate")



;~ call("debugmm", 0xCD0000)
;~ call("debugfs", 0xBD9C0A)

;0xA4C16B - colour of level 1 enemey plant
;0xFAF544 - colour of item on ground
;0xFF0000 - colour of enemy in minimap
;0x7E2914 - colour of enemy in minimap
;722, 65, 737, 80 - area to search around guy to look for enemy on minimap

;------- enemy colour list
; 0xA4C16B - small ball cactus lvl 1
; 0xF0AE6B - tall cactus with flower on top lvl 4 - if this is the value of the colour then you need to click aprox 10 pixels lower
; 0xF2F9A4 - tortoise
;------- object colour list
;0x538BA7 - colour of gray area in health bar when there is no health
;75,183

;huntingscript variables ~~~~~~~
$timemodifier = 200
$xmultiplier = 22
$ymultiplier = 19
$maxrange = 18
;x compensating variables
$xmaxmultiplier = 25  ; 25, - 28 is good for the very top.
$xminmultiplier = 22  ; 18, - but that's not effective
;y compensating variables
$ymaxminimap = 16 ; 16, -
$yminminimap = -13 ;-13,
$ymaxmultiplier = 20
$yminmultiplier = 14
;end of huntingscript variables

$lowhealth = 70
;~ $timemodifier2=40
$maxrange = 60
$multiplier = 3
$failsafewait = 150
;I should put this into an ini file and an array later.
$enemy1 = 0xA4C16B ; lvl 1 plant in shaitan town
$enemy2 = 0xC0D071 ;0xF0AE6B - flower part lvl 4
$enemy3 = 0xF2F9A4 ;tortoise flower lvl 7
$enemy4 = 0xDBD6B8 ; lv8 cuddly sheep
$enemy5 = 0x57B9A9 ; lv9 marsh spirit
$enemy6 = 0xCF973B ; bear cub lvl 12
$enemy7 = 0xD5D5E7 ; lv17 angelic bear
;~ call("debug",$enemy7)

MsgBox(0, "Set coord", "for top left of seach area")
WinActivate("Pirate")
Sleep(250)
$coord = MouseGetPos()
$x1 = $coord[0]
$y1 = $coord[1]
MsgBox(0, "Set coord", "for bottom right of search area")
WinActivate("Pirate")
Sleep(250)
$coord = MouseGetPos()
$x2 = $coord[0]
$y2 = $coord[1]
$t = 0
$shadevar = 0
While $t = 0
	Call("checkandheal")
;~ 	Call("huntingscript")
	Call("enemycoloursearchscript1")
;~ call("wait")
;~ call("waittype2")
call("waittype3")
;~ 	sleep(3500) ;time for body to fade
sleep(1000)
	Send("^a")
	Send("^a")
	Sleep(500)
WEnd

Func wait()
	$xpcheck1 = PixelSearch(74, 43, 185, 50, 0x6DA3B3, 4)
;~ msgbox(0,"1",$xpcheck1[0]&" "&$xpcheck1[1])
	$c = 0
	$w = 0
	While $w = 0	;beautiful procedure, works great:).
		Sleep(50)
		$xpcheck2 = PixelSearch(74, 43, 185, 50, 0x6DA3B3, 4)
		If Not @error Then 	;justincase the pixelsearch doesn't find anything so the script doesn't stop
;~ 		msgbox(0,"2",$xpcheck2[0]&" "&$xpcheck2[1])
			Sleep(50)
			If $xpcheck1[0] <> $xpcheck2[0] Then $w = 1	 ; if no change then it's ok to proceed to next steep
			If $c > $failsafewait Then $w = 1  ;every value of $c is equal to a one loop of sleep(50), this should include estimated time to kill the enemy
			$c = $c + 1
		Else
			$w = 1
		EndIf
		If $c > 5 Then
			PixelSearch(715, 57, 745, 87, 0xFF0000, 10)
			If @error Then $w = 1
		EndIf
	WEnd
EndFunc   ;==>wait

Func waittype2()
	$c = 0
	$w = 0
	While $w = 0	;beautiful procedure, works great:).
		Sleep(500)
		If $c > 5 Then
			PixelSearch(715, 57, 745, 87, 0xFF0000, 10) 	;requires two checks in a row to confirm not attacking enemy
			If @error Then
				Sleep(500)
				PixelSearch(715, 57, 745, 87, 0xFF0000, 10)
				If @error Then $w = 1
			EndIf
		EndIf
		$c = $c + 1
	WEnd
EndFunc   ;==>waittype2

Func waittype3()
	$k=0
	$checksum1 = PixelChecksum($x1, $y1, $x2, $y2)
	$w = 0
	While $w = 0
		Sleep(50)
		$checksum2 = PixelChecksum($x1, $y1, $x2, $y2)
		if $checksum1<>$checksum2 then $w=1
		$k=$k+1
		if $k>40 then $w=1
	WEnd
EndFunc

Func debugmm($colour)
	$shadevar = 0
	$t = 0
	While $t = 0
		Sleep(50)
		$coord = PixelSearch(666, 7, 793, 132, $colour, $shadevar)
		If Not @error Then
			MsgBox(0, "debug script", "coordinates for colour " & Hex($colour, 6) & " with shade variance of " & $shadevar & " are " & $coord[0] & " " & $coord[1])
			$t = 1
		EndIf
		$shadevar = $shadevar + 1
	WEnd
EndFunc   ;==>debugmm

Func debugfs($colour)  ;update to include variables for x and y coord search area
	$shadevar = 0
	$t = 0
	While $t = 0
		Sleep(50)
		$coord = PixelSearch(1, 72, 658, 556, $colour, $shadevar);72
		If Not @error Then
			MsgBox(0, "debug script", "coordinates for colour " & Hex($colour, 6) & " with shade variance of " & $shadevar & " are " & $coord[0] & " " & $coord[1])
			$t = 1
		EndIf
		$coord = PixelSearch(658, 175, 790, 556, $colour, $shadevar)
		If Not @error Then
			MsgBox(0, "debug script", "coordinates for colour " & Hex($colour, 6) & " with shade variance of " & $shadevar & " are " & $coord[0] & " " & $coord[1])
			$t = 1
		EndIf
		$shadevar = $shadevar + 1
	WEnd
EndFunc   ;==>debugfs


Func debug($colour)
	MsgBox(0, "Set coord", "for top left of search area")
	WinActivate("Pirate")
	Sleep(250)
	$coord = MouseGetPos()
	$x1 = $coord[0]
	$y1 = $coord[1]
	MsgBox(0, "Set coord", "for bottom right of search area")
	WinActivate("Pirate")
	Sleep(250)
	$coord = MouseGetPos()
	$x2 = $coord[0]
	$y2 = $coord[1]
	$shadevar = 0
	$t = 0
	While $t = 0
		Sleep(50)
		$coord = PixelSearch($x1, $y1, $x2, $y2, $colour, $shadevar);72
		If Not @error Then
			MsgBox(0, "debug script", "coordinates for colour " & Hex($colour, 6) & " with shade variance of " & $shadevar & " are " & $coord[0] & " " & $coord[1])
			$t = 1
		EndIf
		$shadevar = $shadevar + 1
	WEnd
EndFunc   ;==>debug

Func checkandheal()
	$coord = PixelSearch(75, 26, 183, 31, 0x538BA7)
	If Not @error Then
		$healthlvl= (($coord[0] - 75) / (183 - 75)) * 100
;~ 		MsgBox(0,"location","the x-location of the found pixelis "& $coord[0])
		If $healthlvl < $lowhealth Then
			Send("{INS}")
			$h = 0
			While $h = 0
				Sleep(100)
				PixelSearch(75, 26, 183, 31, 0x538BA7)
				If @error Then
					$h = 1
					Send("{INS}")
				EndIf
			WEnd
		EndIf
	EndIf
EndFunc   ;==>checkandheal


Func huntingscript()
	$a = 0
	$l = 0
	While $l = 0
		$enemysrch = PixelSearch(730 - $a, 72 - $a, 730 + $a, 72 + $a, 0xFF0000) ;note this colour is center of enemy icon on map :) to be precautious you can add a shade variable but I don't think it's nessecary
		If Not @error Then
;~ 			MsgBox(0,"variables","$a="&$a&". $enemysrch[0]="&$enemysrch[0]&" $enemysrch[1]="&$enemysrch[1])
;~ 			mousemove($enemysrch[0]+3,$enemysrch[1]+29,3)
			$l = 1 	; build a cartesian plane for ease of number use
			$kx= ($enemysrch[0] - 730) 	; this is making the coord found relative to the x location center on the mini, will return pos or neg value, like a cartesian plane.
			$ky= (72 - $enemysrch[1]) 	; this makes the 7 coord found relative to the y location center on the mini map.
;~ 			MsgBox(0,"x and y value relative to center","x="&$kx&" y="&$ky)
			$distance= (($kx ^ 2 + $ky ^ 2) ^ (1 / 2))
			$timeout = $distance * $timemodifier ; note, $kx and $ky are now the relation between $enemysrch and the coord 730,72 , which was found to be the center of the arrow on the minimap.
			$xmultiplier = $xminmultiplier + (- ($ky - $ymaxminimap) * (($xmaxmultiplier - $xminmultiplier) / ($ymaxminimap - $yminminimap)))
			$ymultiplier = $yminmultiplier + (- ($ky - $ymaxminimap) * (($ymaxmultiplier - $yminmultiplier) / ($ymaxminimap - $yminminimap)))
;~ 			MsgBox(0,"x and y multipliers are..","xmult="&$xmultiplier&" ymult="&$ymultiplier)
			$kx = $kx * $xmultiplier
			$ky = $ky * $ymultiplier
			; = (a^2 +b^2)^(1/2)
;~ 			MsgBox(0,"variables2","$kx="&$kx&" $ky="&$ky&". $timeout="&$timedistanceout)
;~ 			Mousemove(400+$kx,270-$ky,2)
;~ 			msgbox(0,"here","resulting location is here")
			If $kx < -400 Then $kx = -395
			If $kx > 400 Then $kx = 398
			If $ky < -270 Then $ky = -268
			If $ky > 280 Then $ky = 280
			Send("!m")
			Sleep(100)
			MouseClick("left", 400 + $kx, 270 - $ky, 1, 0)
			Sleep(100)
			Send("!m")
			Sleep(300)
			PixelSearch(180, 10, 637, 550, 0xFF0000, 7)   ;search for red thing over enemy head which appears when engaged/engaging
			If @error Then ;reset variables if the click doesn't work or no enemy is engaged
				$l = 0
				$a = 0
			EndIf
;~ 			send("!m")
			Sleep($timeout) 	;give him enough time to get to the spot on the map according to the distance he is moving
;~ 			msgbox(0,"time","times up with a modifier of "&$timemodifier&" and a resulting time of "&$timeout)
		EndIf
		$a = $a + 1
		If $a >= $maxrange Then $l = 1
	WEnd
EndFunc   ;==>huntingscript

Func enemycoloursearchscript1()
	$shadevar=0
	PixelSearch(71, 62, 741, 82, 0xCD0000, 15) 	;searches area around center of mini map for guys - to see if there are any bad guys near you ; 730,72 is the center of arrow in minimap
;~ 	if @error then
;~ 		call("movetonearestenemy")
;~ 	EndIf
;~ 	PixelSearch(716,58,744,84,0xCD0000,15)
	If Not @error Then
		$s = 0
		While $s = 0
			Sleep(50)  ;old 180,10,637,550
;~ 			$xx1=0; method 2
;~ 			$yy1=0; method 2
;~ 			$xx2=0; method 2
;~ 			$yy2=0; method 2
;~ 			for $i=1 to 20		;10,10,790,550 ; method 2
			$coord = PixelSearch(150, 80, 620, 450, $enemy7, $shadevar)  ; method 1
;~ 			$coord = pixelsearch(400-$xx1,270-$yy1,400+$xx2,270+$yy2,$enemy7,$shadevar) ; method 2 ;searchs for enemy after determining there are guys near you. 180,115,637,476
			If Not @error Then
;~ 				$i=20; method 2
				$s = 1
				$shadevar = 0
			EndIf
;~ 			$xx1=$xx1+12.5; method 2
;~ 			$xx2=$xx2+11; method 2
;~ 			$yy1=$yy1+9.5; method 2
;~ 			$yy2=$yy2+9; method 2
;~ 			Next; method 2
			$shadevar = $shadevar + 1	;increase allowable shade variation by 1 until shade is found
			If $s = 1 Then
				MouseClick("left", $coord[0], $coord[1], 1, 2)
				Sleep(444)
				PixelSearch(180, 10, 637, 550, 0x2C52C9, 12)  ;serach for blue ring to detect if enemy is engaged or being engaged
				If @error Then
					$s = 0
					$shadevar = 0
				EndIf
			EndIf
		WEnd
	EndIf
EndFunc   ;==>enemycoloursearchscript

Func enemycoloursearchscript2()
	If Not @error Then
		$s = 0
		While $s = 0
			Sleep(50)  
			$coord = PixelSearch(1, 1, 790, 560, $enemy7, 7)  ; method 1
			If Not @error Then
				$s = 1
				$shadevar = 0
			EndIf
			If $s = 1 Then
				MouseClick("left", $coord[0], $coord[1], 2, 0)
;~ 				Sleep(444)
;~ 				PixelSearch(180, 10, 637, 550, 0x2C52C9, 12)  ;serach for blue ring to detect if enemy is engaged or being engaged
;~ 				If @error Then
;~ 					$s = 0
;~ 				EndIf
			EndIf
		WEnd
	EndIf
EndFunc


Func _MemoryOpen($iv_Pid, $iv_DesiredAccess = 0x1F0FFF, $if_InheritHandle = 1)

	If Not ProcessExists($iv_Pid) Then
		SetError(1)
		Return 0
	EndIf

	Local $ah_Handle[2] = [DllOpen('kernel32.dll') ]
;~ 	Local $ah_Handle = DllOpen('kernel32.dll') 	-the brackets are because an array is being declared, no brackets neccessary if it was a standalone variable
;~ msgbox(0,"value of ah_Handle[2] is", $ah_Handle[1])
	If @error Then
		SetError(2)
		Return 0
	EndIf
;~ msgbox(0,"value of ah_Handle[0] is", $ah_Handle[0]&"inherithandle"&$if_InheritHandle&"and the iv_pid"&$iv_Pid)
	Local $av_OpenProcess = DllCall($ah_Handle[0], 'int', 'OpenProcess', 'int', $iv_DesiredAccess, 'int', $if_InheritHandle, 'int', $iv_Pid)
;~ 	Local $av_OpenProcess = DllCall('kernel32.dll', 'int', 'OpenProcess', 'int', 0x1F0FFF(all access), 'int', 1, 'int', 4084)
;~ msgbox(0,"value of $av_openprocess is", $av_OpenProcess[0]) ;1820
;~ msgbox(0,"value of iv_pid is", $iv_Pid) ; 4084
	If @error Then
		DllClose($ah_Handle[0])
		SetError(3)
		Return 0
	EndIf

	$ah_Handle[1] = $av_OpenProcess[0]
;~ msgbox(0,"value of ah_Handle is", $ah_Handle)
	Return $ah_Handle

EndFunc   ;==>_MemoryOpen


Func _MemoryRead($ah_Handle, $iv_Address, $sv_Type = 'dword')

	If Not IsArray($ah_Handle) Then
		SetError(1)
		Return 0
	EndIf

	Local $v_Buffer = DllStructCreate($sv_Type)

	If @error Then
		SetError(@error + 1)
		Return 0
	EndIf

;~ 	DllCall($ah_Handle[0], 'int', 'ReadProcessMemory', 'int', $iv_Address, 'int', $ah_Handle[1], 'int', DllStructGetSize($v_Buffer), 'int', '', 'ptr', DllStructGetPtr($v_Buffer))
	DllCall($ah_Handle[0], 'int', 'ReadProcessMemory', 'int', $ah_Handle[1], 'int', $iv_Address, 'ptr', DllStructGetPtr($v_Buffer), 'int', DllStructGetSize($v_Buffer), 'int', '')
;~ msgbox(0,"value of ah_Handle[0],[1] is", $ah_Handle[0]&", "&$ah_Handle[1]);1,1788 
	If Not @error Then
		Local $v_Value = DllStructGetData($v_Buffer, 1)
		Return $v_Value
	Else
		SetError(6)
		Return 0
	EndIf

EndFunc   ;==>_MemoryRead


Func _MemoryWrite($ah_Handle, $iv_Address, $v_Data, $sv_Type = 'dword')

	If Not IsArray($ah_Handle) Then
		SetError(1)
		Return 0
	EndIf

	Local $v_Buffer = DllStructCreate($sv_Type)

	If @error Then
		SetError(@error + 1)
		Return 0
	Else
		DllStructSetData($v_Buffer, 1, $v_Data)
		If @error Then
			SetError(6)
			Return 0
		EndIf
	EndIf

	DllCall($ah_Handle[0], 'int', 'WriteProcessMemory', 'int', $ah_Handle[1], 'int', $iv_Address, 'ptr', DllStructGetPtr($v_Buffer), 'int', DllStructGetSize($v_Buffer), 'int', '')

	If Not @error Then
		Return 1
	Else
		SetError(7)
		Return 0
	EndIf

EndFunc   ;==>_MemoryWrite


Func _MemoryClose($ah_Handle)

	If Not IsArray($ah_Handle) Then
		SetError(1)
		Return 0
	EndIf

	DllCall($ah_Handle[0], 'int', 'CloseHandle', 'int', $ah_Handle[1])
	If Not @error Then
		DllClose($ah_Handle[0])
		Return 1
	Else
		DllClose($ah_Handle[0])
		SetError(2)
		Return 0
	EndIf

EndFunc   ;==>_MemoryClose

Func Read()
	$M_open = _MemoryOpen($Process)
	$Players = _MemoryRead($M_open, "0x" & $Mem_Players)
	$Score_p1 = _MemoryRead($M_open, "0x" & $Mem_Score_p1)
	;$Balls_p1 = _MemoryPointerRead($M_open, "0x" & "00C4AE9E", 0)
	;$Balls_p1 = _MemoryRead($M_open, $Mem_Balls_p1)


	$Balls_p1a = _MemoryRead($M_open, "0x" & $Mem_Balls_p1)
	$Balls_p1b = '0x' & Hex($Balls_p1a + $Mem_Balls_p1_offset)
	$Balls_p1 = _MemoryRead($M_open, $Balls_p1b)
	_MemoryClose($M_open)

	;$Players = Asc($Playersa)
	;$Balls_p1 = Abs(Asc($Balls_p1a) - 3)
	;$Score_p1 = Dec($Score_p1a)

	;If StringLen ($Score_p1a) > 6 Then
	;   $Score_p1 = _StringInsert ($Score_p1a, "___", 2)
	;Else
	;   $Score_p1 = $Score_p1a
	;EndIf
EndFunc   ;==>Read

Func Score_p1_Write()
	If $Score_p1_freeze_c1 = 1 Then
		$Score_p1_Read = $freeze_score_p1
		$Score_p1_freeze_c1 = 0
		ConsoleWrite($freeze_score_p1 & @CR)
	Else
		$Score_p1_Read = GUICtrlRead($Input_p1_Score_edit)
	EndIf

	$M_open = _MemoryOpen($Process)
;~ 	msgbox(0,"value of $M_open is", $M_open[0])
	_MemoryWrite($M_open, "0x" & $Mem_Score_p1, "0x" & Hex($Score_p1_Read))
	_MemoryClose($M_open)
	$freeze_score_p1 = $Score_p1_Read
EndFunc   ;==>Score_p1_Write
;~ Func movetonearestenemy()   ;use rise over run to figure out where to click on screen to get to enemy, don't actually use decimal though, make relative to center.
;~ $a=0
;~ $l=0
;~ while $l=0
;~ 	$enemysrch=PixelSearch(716-$l,58-$l,744+$l,84+$l,0xFF0000,2) ;note this colour is center of enemy icon on map :) to be precautious you can add a shade variable but I don't think it's nessecary
;~ 	MsgBox(0,"variables","$l="&$l&". $enemysrch[0]="&$enemysrch[0]&" $enemysrch[1]="&$enemysrch[1])
;~ 	if not @error Then
;~ 		MsgBox(0,"variables","$l="&$l&". $enemysrch[0]="&$enemysrch[0]&" $enemysrch[1]="&$enemysrch[1])
;~ 		$l=1					; build a cartesian plane for ease of number use
;~ 		$kx=$enemysrch[0]-730	; this is making the coord found relative to the x location center on the mini, will return pos or neg value, like a cartesian plane.
;~ 		$ky=$enemysrch[1]-72	; this makes the 7 coord found relative to the y location center on the mini map.
;~ 		; note, $kx and $ky are now the relation between $enemysrch and the coord 730,72 , which was found to be the center of the arrow on the minimap.
;~ 		$kx=$kx*$multiplier
;~ 		$ky=$ky*$multiplier
;~ 		$timeout=(($kx^2+$ky^2)^(1/2))*$timemodifier2  				;distance = (a^2 +b^2)^(1/2)
;~ 		MsgBox(0,"variables2","$kx="&$kx&" $ky="&$ky&". $timeout="&$timeout)
;~ 		MouseClick("left",400+$kx,270+$ky,2,2)
;~ 		sleep($timeout)					;give him enough time to get to the spot on the map according to the distance he is moving
;~ 	EndIf
;~ 	$a=$a+1
;~ 	if $a>=$maxrange then $l=1
;~ WEnd
;~ EndFunc

;center of minimap is 730, 72
;center of character is 400,270

;~ Func pickupitems()	; old function - not important

;800x600 is screen size (heightxwidth)
;character is located at (400,270) - at that clicking location he will not move
;8 coordinates are nessecary, upright, up, up left, left, downleft, down, downright, right. more could be used however. as long as it is relative to mini map... perhaps a formula could be used.


;------math
;96 is the lowest point (value of y) of the arrow on minimap and 48 is the highest. the center is in the middle.
;706 is the lowest point (value of x) and 754 is the highest. therefore the box is square, 48 x 48. the center would be...
; ((x2-x1)/2,(y2-y1)/2) (754-706)/2= 24 for the x and (96-48)/2=24 for the y...
;the center is located at, (730,72)~~~~~<<<<---------------
; monsters within a radius of 10 pixels on the minimap are definetly seen