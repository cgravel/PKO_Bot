#include <GUIConstants.au3>
#include <GuiConstantsEx.au3>
#include <EditConstants.au3>
#include <GuiSlider.au3>
#include <GUIListBox.au3>
#include <Misc.au3>
#include <WindowsConstants.au3>
#include <StaticConstants.au3>
#include <ButtonConstants.au3>
#include <GUIOnChangeRegister.au3>


Opt("pixelcoordmode", 2)
Opt("mousecoordmode", 2)
Opt("GUIOnEventMode", 1)
Opt("WinTitleMatchMode", 4)
Opt("MouseClickDelay", 20)
Opt("MouseClickDownDelay", 30)
Opt("MouseClickDragDelay", 250)
$title = "KOP2.0"
$title_b = "KOP2.0"
$title_c = "PKO Trainer by crisli_ve with very special thanks to Labrynth"
$Process = WinGetProcess($title_b, "")
_Singleton("PKO Bot", 0) ; This is so only one occurance of the bot may run at a time

#Region preset variables
#Region colours
Global $enemylvlsame = 0XFFFFFF
Global $enemylvllower = 0xD9FF77 ; 3-4 levels lower
Global $enemylvllow = 0x00FF0C ;23 and 28 - 5 levels lower
Global $enemylvlverylow = 0xC0C0C0 ;
Global $enemytoolowlvl = 0xC6C3C6 ;
Global $characteractioncolor = 0xc4ec3c ; the colour of the character action things
;~ 0xF70CF7 purple
#EndRegion colours

#Region important memory values values
; Note that each value corresponds to a 4 character text which is reversed... convert to Hex then reverse and take the ASCII... I don't remember why it's reversed though X_x
Global $Dual_Weapon = 1818326340 ;185 *values for new way to find equipped weapon
Global $Greatsword = 1634038343 ;141
Global $Sword = 1919907667 ;137
Global $Bow = 7827266 ;146
Global $Firegun = 1701996870 ;151
Global $Dagger = 1734828356
Global $Short_Staff = 1919903827
Global $Bare_Hand = 1701994818 ;122
Global $xpgain = 542136389
#EndRegion important memory values values

Global $filepath
Global $sIni = @DesktopDir & "\PKO.ini"
Global $sStr = FileRead("PKOItemList.txt")
Global $screenresolution
Global $currentweapon
Global $maxwindowx = 0
Global $maxwindowy = 0
Global $searchymin = 0
Global $searchymax = 0
Global $searchxmin = 0
Global $searchxmax = 0
Global $engaged
Global $handle = ControlGetHandle($title_b, "", "")
Global $stand ; Generic stand value
Global $playback = 0
Global $pixelsearchstep = 2

Global $hBox = -1 ; not created
Global $boxoffsetx = 350 ; offset of x -- where the picture begins
Global $boxoffsety = 50 ; offset of y -- where the picture begins
Global $boxtotalheight = 225
Global $boxtotalwidth = 300

Global $boxleft = 0 ; left
Global $boxtop = 0 ; top
Global $boxwidth = 50 ; width
Global $boxheight = 50 ; height

Dim $boxdim[4] ;this is the array used for drawing the box in the attack tab of the GUI
Dim $inventorybuttonsGUI[32]

#Region memory addresses
Global $mem_weapon = "00794cbd" ; 1 byte value, values above of the weapons
Global $mem_screenresolution = "007788f0" ; find by searching the current resolution ex. 1024 or 800
Global $mem_xcoord = "0079a8f0" ; find by searching the coordinates... this is a float value, y is less accurate and is always 4 bytes infront
Global $mem_ycoord = "0079a8f4"
Global $mem_xcoordNav = "007882b0" ; Find this by searching for the value of the xcoordinate after using the auto-navigate function; it has it's own dedicated value
Global $mem_ycoordNav = "007882b4"
Global $mem_xcoordblinkercursor = "00788938" ; Find this by playing with a menu that has a blinkercursor and searching the changes
Global $mem_ycoordblinkercursor = "0078893c"
Global $mem_inventorymenuscrollposition = "00788914" ;16 paling atas, $inventorysize paling bawah
Global $mem_menucount = "00794774" ; the number of menus open


Global $mem_lastaction = "007884a0" ; search the text of the weapon after attacking

; After locating an item, find the slot number which it is in and then do a pointer search for that address that contains the slot number. Afterwards you will notice that you come to a place in the code where there is an array of pointers for each items slot number. Find the base pointer a viola, from the item's slot number you can find it's type and quantity via offset
; lines of code in memory which will help locate this are:
; game.exe+34C05:Code :mov [eax+edx*4],ebx Moving item into a slot ; this is for inventory above 24 slots... just find eax and work backwards
; game.exe+34C0E:Code :mov [eax+esi*4],edi pointer to location of slots ; this is for inventory below 24 slots... find eax and work backwards
Global $mem_cursoroveritem = "00794590" ; 2 byte item type
Global $mem_cursoroveritemquantity = "00794592" ; 2 byte item quantity

Global $mem_PTRcoordstrademenu = "007927d0"
Global $mem_PTRcoordsinventorymenu = "0078d8dc" ; this is right behind the basepointer for inventory
Global $mem_PTRcoordswalktomenu = "00793984"
Global $mem_PTRhealth = "0079a72c" ; search for health and search for a pointer
Global $mem_PTRexperience = "00792abc" ; search for experience by searching for how much the experience increased by
Global $mem_PTRinventorysize
Global $mem_PTRinventory
Global $mem_basePTRinventory = "0078d8e0" ; search for this by searching for a 4byte of an item and it's amount, ex, item code "3122 is elven fruit juice and it's quantity is 3 so search for the combined 4 byte value to get a unique number.


Global $cursoroveritem = "00794590" ; find this by using the item codes and by searching for a particular item code after the mouse has hovered over the item and the ctrltip displayed
Global $mem_offsethealth = "d8"
Global $mem_offsethealthmax = "1c8"
Global $mem_offsetexperience = "148"
Global $mem_offsetPTRinventory = "b0" ; character 1
Global $mem_offsetinventorysize = "d4"
Global $mem_offsetinventory = "40"
Global $mem_offsetcoordsinventorymenu = "128"
Global $mem_offsetcoordswalktomenu = "128"
Global $mem_offsetcoordstrademenu = "128"


Global $mem_health
Global $mem_healthmax
Global $mem_experience
Global $mem_xcoordinventorymenu ;= "00788920" ; Find this by playing with the menu location and searching for its coords
Global $mem_ycoordinventorymenu ;= "0078891c"
Global $mem_xcoordwalktomenu
Global $mem_ycoordwalktomenu
Global $mem_xcoordtrademenu
Global $mem_ycoordtrademenu

Global $mem_cursorid = "007784ac"
;0 - 	Regular pointer cursor
;6 -    Pocket watch cursor - for game loading
;7 -	Attack cursor (sword)
;8 - 	Skill activate cursor
;9 - 	Marching cursor
;10 - 	Boat cursor to dock
;14 -	Adjust view cursor
;15 -   Dialog box cursor
;16 -   Mouse cursor that's a mouse
;20 -	Boots in a crossed circle


Global $xcoord ; This is for the value returned from float value location of the character -- this value is not always accurate depending on method of travel
Global $ycoord
Global $xcoordNav ; This value is used during the auto-navigate function; it is a dedicated value
Global $ycoordNav
Global $xcoordblinkercursor ; This is the xcoord of the walktomenu
Global $ycoordblinkercursor
Global $xcoordinventorymenu ; This is the xcoord of the inventorymenu
Global $ycoordinventorymenu
Global $xcoordwalktomenu
Global $ycoordwalktomenu
Global $xcoordtrademenu
Global $ycoordtrademenu
Global $startinventorytobesold
Global $inventorymenuscrollposition
Global $lastaction ; the last action that happened
Global $health ; current health
Global $healthmax ; max health
Global $menucount ;number of open menus
Global $cursorid ; the id of the cursor
Global $enemycoord ; this is what has been found from the pixelsearch
Global $experience
Global $cursoroveritem ;this is the current item the cursor is over
Global $cursoroveritemquantity ; this is the quantity of the item the cursor is over
Global $inventorysize ; the size of the inventory

Dim $inventory[32][4] ; 32 spots; itempointer, typeraw, quantity and return of stringregexp
Global $inventoryfull = 0



#EndRegion memory addresses
#EndRegion preset variables
Global $flee_countercheck
Global $n = 0


#Region Hotkey List
HotKeySet("{end}", "stopbot")
HotKeySet("q", "closeprogram")
#EndRegion Hotkey List

#Region GUI
Global $guimaxy = 350
Global $guimaxx = 700
Global $handleGUI = GUICreate($title_c, $guimaxx, $guimaxy, -1, -1)
Global $handleGUIchild = GUICreate("Inventory", 300, 544, @DesktopWidth - 310, 50, -1, -1, $handleGUI)
Global $inventorylistGUI = GUICtrlCreateList("", 0, 0, 300, 100, ($GUI_SS_Default_list - $LBS_SORT))
GUICtrlSetData(-1, "|||||||||||||||||||||||||||||||||")

GUISwitch($handleGUI)
#Region Bot Tab
GUISwitch($handleGUI)

GUISetIcon(@SystemDir & "\mspaint.exe", 0)
GUISetOnEvent($GUI_EVENT_CLOSE, "Quit")

GUICtrlSetOnEvent(-1, "Quit")

$tab = GUICtrlCreateTab(0, 0, 700, 25)

$tabbot = GUICtrlCreateTabItem("Bot")
GUICtrlCreateButton("Exit", 550, $guimaxy - 30, 150, 30)
GUICtrlSetOnEvent(-1, "Quit")
$startbot = GUICtrlCreateButton("Start Botting", $guimaxx - 150, $guimaxy - 60, 150, 30)
GUICtrlSetOnEvent(-1, "startbot")
GUICtrlSetTip($startbot, "Start running the bot.", "Start botting")



$savedattackzonelistGUI = GUICtrlCreateList("", $guimaxx - 700, 80, 120, 170, ($LBS_STANDARD))
GUICtrlSetOnEvent(-1, "putnametoedit")
GUICtrlCreateButton("Save", $guimaxx - 580, 50, 70, 30)
GUICtrlSetOnEvent(-1, "Savesaved")
GUICtrlCreateButton("Load", $guimaxx - 580, 80, 70, 30)
GUICtrlSetOnEvent(-1, "Loadsaved")
GUICtrlCreateButton("Add", $guimaxx - 580, 110, 70, 30)
GUICtrlSetOnEvent(-1, "Addslot")
GUICtrlCreateButton("Delete", $guimaxx - 580, 220, 70, 30)
GUICtrlSetOnEvent(-1, "Deleteslot")
$savednameeditGUI = GUICtrlCreateInput("", $guimaxx - 700, 50, 120, 30)


GUICtrlCreateGroup("Attack Hot Spots", $guimaxx - 330, 35, 315, 220)

$attackzonelistGUI = GUICtrlCreateList("", $guimaxx - 150, 50, 100, 200, ($LBS_STANDARD - $LBS_SORT))
$moveupGUI = GUICtrlCreateButton("^", $guimaxx - 50, 50, 30, 30)
GUICtrlSetOnEvent(-1, "movelistitemup")
$movedownGUI = GUICtrlCreateButton("v", $guimaxx - 50, 80, 30, 30)
GUICtrlSetOnEvent(-1, "movelistitemdown")

GUICtrlCreateButton("Remove Selected", $guimaxx - 300, 80, 150, 30)
GUICtrlSetOnEvent(-1, "remove")
GUICtrlCreateButton("Add Current Position", $guimaxx - 300, 50, 150, 30)
GUICtrlSetOnEvent(-1, "addcurrentpositionhotspot")

GUICtrlCreateGroup("Manually add Hot Spot:", $guimaxx - 300, 120, 140, 70)
$xAttackzoneGUI = GUICtrlCreateInput("", $guimaxx - 290, 135, 60, 20, 0x2000)
$yAttackzoneGUI = GUICtrlCreateInput("", $guimaxx - 230, 135, 60, 20, 0x2000)
GUICtrlCreateGroup("", -99, -99, 1, 1)

GUICtrlCreateButton("Add", $guimaxx - 290, 155, 60, 20)
GUICtrlSetOnEvent(-1, "manuallyaddattackzone")
GUICtrlCreateButton("Clear", $guimaxx - 220, 220, 70, 30)
GUICtrlSetOnEvent(-1, "clearlist")
GUICtrlCreateGroup("", -99, -99, 1, 1)


GUICtrlCreateGroup("Safe Rest Zone", $guimaxx - 480, 35, 140, 130)
$xSafezoneGUI = GUICtrlCreateInput("", $guimaxx - 475, 60, 60, 20, 0x2000)
$ySafezoneGUI = GUICtrlCreateInput("", $guimaxx - 405, 60, 60, 20, 0x2000)
GUICtrlCreateButton("V", $guimaxx - 415, 90, 20, 20)
GUICtrlSetOnEvent(-1, "manuallyaddsafezone")
$safezoneGUI = GUICtrlCreateLabel("Not Set", $guimaxx - 440, 110, 80, 20)
GUICtrlCreateButton("Add Current Position", $guimaxx - 475, 130, 130, 30)
GUICtrlSetOnEvent(-1, "addcurrentpositionsafezone")
GUICtrlCreateGroup("", -99, -99, 1, 1)

$saveoptions = GUICtrlCreateButton("Save Defaults", $guimaxx - 150, $guimaxy - 90, 150, 30)
GUICtrlSetOnEvent(-1, "setoptionsasdefault")
GUICtrlSetTip($saveoptions, "Save options as default.", "Save default")

GUICtrlCreateButton("Save1", 220, $guimaxy - 170, 70, 30)
GUICtrlSetOnEvent(-1, "Save1")
GUICtrlCreateButton("Load1", 220, $guimaxy - 140, 70, 30)
GUICtrlSetOnEvent(-1, "Load1")

GUICtrlCreateButton("Save2", 290, $guimaxy - 170, 70, 30)
GUICtrlSetOnEvent(-1, "Save2")
GUICtrlCreateButton("Load2", 290, $guimaxy - 140, 70, 30)
GUICtrlSetOnEvent(-1, "Load2")


GUICtrlCreateTabItem("")
#EndRegion Bot Tab
#Region Bot Functions
Func startbot()

	Readpointers() ; Find the value of certain memory addresses by reading the pointers
	$experience = Readexperience()

	Readscreenresolution()
	Readweapon()
	Readinventory()

	setoptions()

	$programstart = 1

	resetengage()
	GUISetState(@SW_SHOW, $handleGUIchild)

	MsgBox(0, "Botting", "Botting will begin shortly; now go grab some tea and read a book... " & @CRLF & "People don't read enough books these days.")
EndFunc   ;==>startbot

Func stopbot()
	$programstart = 0
	ControlSend($title_b, "", $handle, "{shiftup}")

	GUISetState(@SW_HIDE, $handleGUIchild)

EndFunc   ;==>stopbot

Func closeprogram()
	Exit
EndFunc   ;==>closeprogram

Func readGUIhotkeys()
	$GUI_hotkeys[0] = IniRead($sIni, "GUI hotkeys", "Start Bot", "no hotkey set") ;load macro information to macro array
	$GUI_hotkeys[1] = IniRead($sIni, "GUI hotkeys", "Stop Bot", "no hotkey set")
	If $GUI_hotkeys[0] <> "no hotkey set" Then
		HotKeySet($GUI_hotkeys[0], "startbot")
		If StringLeft($GUI_hotkeys[0], 1) <> "+" Then
			HotKeySet("+" & $GUI_hotkeys[0], "startbot")
		EndIf
	EndIf
	If $GUI_hotkeys[1] <> "no hotkey set" Then
		HotKeySet($GUI_hotkeys[1], "stopbot")
		If StringLeft($GUI_hotkeys[1], 1) <> "+" Then
			HotKeySet("+" & $GUI_hotkeys[1], "stopbot")
		EndIf
	EndIf
EndFunc   ;==>readGUIhotkeys

Func setoptions()

	$auto_sit = GUICtrlRead($auto_sitGUI)
	$flee_healthlevel = GUICtrlRead($flee_healthlevelGUI)
	$emergency_healthlevel = GUICtrlRead($emergency_healthlevelGUI)
	$xSafezone = Number(StringLeft(GUICtrlRead($safezoneGUI), 4))
	$ySafezone = Number(StringRight(GUICtrlRead($safezoneGUI), 4))
	$xsellercoord = Number(StringLeft(GUICtrlRead($sellercoordsGUI), 4))
	$ysellercoord = Number(StringRight(GUICtrlRead($sellercoordsGUI), 4))
	$startinventorytobesold = GUICtrlRead($startinventorytobesoldGUI)
	$sellinventory = GUICtrlRead($sellinventoryGUI)
	$failsafewalktotime = GUICtrlRead($failsafewalktotimeGUI)
	$failsafeattackzone = GUICtrlRead($failsafeattackzoneGUI)
	$failsafexpwaittime = GUICtrlRead($failsafexpwaittimeGUI)

	Select
		Case GUICtrlRead($radiocloserangeGUI) = 1
			$defaultattack = 1
			$searchymax = ($maxwindowy * 26 / 40)
			$searchymin = ($maxwindowy / 2 * 35 / 64)
			$searchxmin = ($maxwindowx * 3 / 8)
			$searchxmax = ($maxwindowx * 5 / 8)
		Case GUICtrlRead($radiofullrangeGUI) = 1
			$defaultattack = 2
			$searchymax = $maxwindowy * (7 / 8)
			$searchymin = $maxwindowy * (1 / 5)
			$searchxmin = $maxwindowx * (1 / 5)
			$searchxmax = $maxwindowx * (4 / 5)
		Case GUICtrlRead($radiocustomrangeGUI) = 1
			$defaultattack = 3
			$searchxmin = $maxwindowx * (($boxdim[0] - $boxoffsetx) / $boxtotalwidth)
			$searchymin = $maxwindowy * (($boxdim[1] - $boxoffsety) / $boxtotalheight)
			$searchxmax = $maxwindowx * ((($boxdim[2]) + ($boxdim[0] - $boxoffsetx)) / $boxtotalwidth)
			$searchymax = $maxwindowy * ((($boxdim[3]) + ($boxdim[1] - $boxoffsety)) / $boxtotalheight)
	EndSelect

	Dim $attackzonelist[_GUICtrlListBox_GetCount($attackzonelistGUI)]
	Dim $xAttackzone[_GUICtrlListBox_GetCount($attackzonelistGUI)]
	Dim $yAttackzone[_GUICtrlListBox_GetCount($attackzonelistGUI)]
	For $i = 0 To (UBound($attackzonelist) - 1)
		$attackzonelist[$i] = _GUICtrlListBox_GetText($attackzonelistGUI, $i)
		$xAttackzone[$i] = Number(StringLeft($attackzonelist[$i], 4))
		$yAttackzone[$i] = Number(StringRight($attackzonelist[$i], 4))
	Next
EndFunc   ;==>setoptions

Func setoptionsasdefault()

	Select
		Case GUICtrlRead($radiocloserangeGUI) = 1
			IniWrite($sIni, "defaults", "attack", 1)
		Case GUICtrlRead($radiofullrangeGUI) = 2
			IniWrite($sIni, "defaults", "attack", 1)
		Case GUICtrlRead($radiocustomrangeGUI) = 1
			IniWrite($sIni, "defaults", "attack", 3)
	EndSelect

	For $i = 0 To 3
		IniWrite($sIni, "custombox", $i, $boxdim[$i])
	Next


	If (_GUICtrlListBox_GetCount($attackzonelistGUI) <> 0) Then
		For $i = 0 To (_GUICtrlListBox_GetCount($attackzonelistGUI) - 1)
			IniWrite($sIni, "defaults", "attackzone" & $i, _GUICtrlListBox_GetText($attackzonelistGUI, $i))
		Next
		IniWrite($sIni, "defaults", "attackzonearraysize", _GUICtrlListBox_GetCount($attackzonelistGUI))
	Else
		IniWrite($sIni, "defaults", "attackzonearraysize", 1)
		IniWrite($sIni, "defaults", "attackzone0", "empty")
	EndIf
	IniWrite($sIni, "defaults", "safezone", GUICtrlRead($safezoneGUI))
	IniWrite($sIni, "defaults", "attackclose", GUICtrlRead($radiocloserangeGUI))
	IniWrite($sIni, "defaults", "attackfull", GUICtrlRead($radiofullrangeGUI))

	IniWrite($sIni, "defaults", "sellercoords", GUICtrlRead($sellercoordsGUI))
	IniWrite($sIni, "defaults", "sellinventory", GUICtrlRead($sellinventoryGUI))
	IniWrite($sIni, "defaults", "startinventorytobesold", GUICtrlRead($startinventorytobesoldGUI))

	IniWrite($sIni, "defaults", "flee_healthlevel", $flee_healthlevel)
	IniWrite($sIni, "defaults", "auto_sit", GUICtrlRead($auto_sitGUI))
	IniWrite($sIni, "defaults", "emergency_healthlevel", GUICtrlRead($emergency_healthlevelGUI))

	IniWrite($sIni, "defaults", "failsafewalktotime", GUICtrlRead($failsafewalktotimeGUI))
	IniWrite($sIni, "defaults", "failsafeattackzone", GUICtrlRead($failsafeattackzoneGUI))
	IniWrite($sIni, "defaults", "failsafexpwaittime", GUICtrlRead($failsafexpwaittimeGUI))



	MsgBox(0, "Alert", "defaults are set")
EndFunc   ;==>setoptionsasdefault
#Region Unimportant botstart
Func manuallyaddsafezone()
	GUICtrlSetData($safezoneGUI, GUICtrlRead($xSafezoneGUI) & ", " & GUICtrlRead($ySafezoneGUI))
EndFunc   ;==>manuallyaddsafezone

Func manuallyaddattackzone()
	Local $msg
	$msg = GUICtrlRead($xAttackzoneGUI) & ", " & GUICtrlRead($yAttackzoneGUI)
	_GUICtrlListBox_AddString($attackzonelistGUI, $msg)
EndFunc   ;==>manuallyaddattackzone

Func addcurrentpositionhotspot()
	Readlocation()
	_GUICtrlListBox_AddString($attackzonelistGUI, $xcoord & ", " & $ycoord)
EndFunc   ;==>addcurrentpositionhotspot

Func addcurrentpositionsafezone()
	Readlocation()
	GUICtrlSetData($safezoneGUI, $xcoord & ", " & $ycoord)
EndFunc   ;==>addcurrentpositionsafezone

Func movelistitemup()
	Local $cursel = _GUICtrlListBox_GetCurSel($attackzonelistGUI)
	If ($cursel) <> 0 Then
		_GUICtrlListBox_SwapString($attackzonelistGUI, $cursel, $cursel - 1)
		_GUICtrlListBox_SetCurSel($attackzonelistGUI, $cursel - 1)
	EndIf
EndFunc   ;==>movelistitemup

Func movelistitemdown()
	Local $cursel = _GUICtrlListBox_GetCurSel($attackzonelistGUI)
	If ($cursel) < (_GUICtrlListBox_GetCount($attackzonelistGUI) - 1) Then
		_GUICtrlListBox_SwapString($attackzonelistGUI, $cursel, $cursel + 1)
		_GUICtrlListBox_SetCurSel($attackzonelistGUI, $cursel + 1)
	EndIf
EndFunc   ;==>movelistitemdown

Func updatelist()
	Dim $attackzonelist[_GUICtrlListBox_GetCount($attackzonelistGUI)]
	For $i = 0 To (UBound($attackzonelist) - 1)
		$attackzonelist[$i] = _GUICtrlListBox_GetText($attackzonelistGUI, $i)
	Next
EndFunc   ;==>updatelist

Func remove()
	Local $cursel = _GUICtrlListBox_GetCurSel($attackzonelistGUI)
	_GUICtrlListBox_DeleteString($attackzonelistGUI, $cursel)
	_GUICtrlListBox_SetCurSel($attackzonelistGUI, $cursel)
EndFunc   ;==>remove

Func clearlist()
	For $i = 0 To (_GUICtrlListBox_GetCount($attackzonelistGUI) - 1)
		_GUICtrlListBox_DeleteString($attackzonelistGUI, 0)
	Next
EndFunc   ;==>clearlist

Func Save1()
	If (_GUICtrlListBox_GetCount($attackzonelistGUI) <> 0) Then
		For $i = 0 To (_GUICtrlListBox_GetCount($attackzonelistGUI) - 1)
			IniWrite($sIni, "save1", "attackzone" & $i, _GUICtrlListBox_GetText($attackzonelistGUI, $i))
		Next
		IniWrite($sIni, "save1", "attackzonearraysize", _GUICtrlListBox_GetCount($attackzonelistGUI))
	Else
		IniWrite($sIni, "save1", "attackzonearraysize", 1)
		IniWrite($sIni, "save1", "attackzone0", "empty")
	EndIf
	IniWrite($sIni, "save1", "safezone", GUICtrlRead($safezoneGUI))
EndFunc   ;==>Save1

Func Save2()
	If (_GUICtrlListBox_GetCount($attackzonelistGUI) <> 0) Then
		For $i = 0 To (_GUICtrlListBox_GetCount($attackzonelistGUI) - 1)

			IniWrite($sIni, "save2", "attackzone" & $i, _GUICtrlListBox_GetText($attackzonelistGUI, $i))
		Next
		IniWrite($sIni, "save2", "attackzonearraysize", _GUICtrlListBox_GetCount($attackzonelistGUI))
	Else
		IniWrite($sIni, "save2", "attackzonearraysize", 1)
		IniWrite($sIni, "save2", "attackzone0", "empty")
	EndIf
	IniWrite($sIni, "save2", "safezone", GUICtrlRead($safezoneGUI))
EndFunc   ;==>Save2

Func Load1()
	clearlist()
	$xSafezone = Number(StringLeft(IniRead($sIni, "save1", "safezone", ""), 4))
	$ySafezone = Number(StringRight(IniRead($sIni, "save1", "safezone", ""), 4))
	GUICtrlSetData($safezoneGUI, $xSafezone & ", " & $ySafezone)

	Dim $attackzonelist[IniRead($sIni, "save1", "attackzonearraysize", 1)]
	If (IniRead($sIni, "save1", "attackzone0", "empty") <> "empty") Then
		For $i = 0 To (UBound($attackzonelist) - 1)
			$attackzonelist[$i] = IniRead($sIni, "save1", "attackzone" & $i, "")
			_GUICtrlListBox_AddString($attackzonelistGUI, $attackzonelist[$i])
		Next
	EndIf

EndFunc   ;==>Load1

Func Load2()
	clearlist()
	$xSafezone = Number(StringLeft(IniRead($sIni, "save2", "safezone", ""), 4))
	$ySafezone = Number(StringRight(IniRead($sIni, "save2", "safezone", ""), 4))
	GUICtrlSetData($safezoneGUI, $xSafezone & ", " & $ySafezone)

	Dim $attackzonelist[IniRead($sIni, "save2", "attackzonearraysize", 1)]
	If (IniRead($sIni, "save2", "attackzone0", "empty") <> "empty") Then
		For $i = 0 To (UBound($attackzonelist) - 1)
			$attackzonelist[$i] = IniRead($sIni, "save2", "attackzone" & $i, "")
			_GUICtrlListBox_AddString($attackzonelistGUI, $attackzonelist[$i])
		Next
	EndIf

EndFunc   ;==>Load2


Func checkselected()
	_GUICtrlListBox_SetCurSel($savedattackzonelistGUI, _GUICtrlListBox_FindString($savedattackzonelistGUI, GUICtrlRead($savednameeditGUI), True))

EndFunc   ;==>checkselected

Func putnametoedit()

	GUICtrlSetData($savednameeditGUI, GUICtrlRead($savedattackzonelistGUI))

EndFunc   ;==>putnametoedit


Func Savesaved()
	Local $savedname = GUICtrlRead($savednameeditGUI)
	Local $cursel = _GUICtrlListBox_GetCurSel($savedattackzonelistGUI)
	Local $getcount = _GUICtrlListBox_GetCount($savedattackzonelistGUI)
	Local $readedit = GUICtrlRead($savednameeditGUI)
	Local $readlistbox = GUICtrlRead($savedattackzonelistGUI)
	Select
		Case $cursel = -1
			MsgBox(0x10, "Error", "No save selected")
			Return
		Case $readedit = ""
			MsgBox(0, "Error", "Please create a unique name in the input box")
			Return

		Case _GUICtrlListBox_FindString($savedattackzonelistGUI, $readedit, True) <> -1
			_GUICtrlListBox_SetCurSel($savedattackzonelistGUI, _GUICtrlListBox_FindString($savedattackzonelistGUI, $readedit, True))
		Case Else
			IniRenameSection($sIni, GUICtrlRead($savedattackzonelistGUI), $readedit)
			IniWrite($sIni, "savednames", $cursel + 1, $readedit)
			_GUICtrlListBox_ReplaceString($savedattackzonelistGUI, $cursel, $readedit)
	EndSelect
	If (_GUICtrlListBox_GetCount($attackzonelistGUI) <> 0) Then
		For $i = 0 To (_GUICtrlListBox_GetCount($attackzonelistGUI) - 1)
			IniWrite($sIni, $savedname, "attackzone" & $i, _GUICtrlListBox_GetText($attackzonelistGUI, $i))
		Next
		IniWrite($sIni, $savedname, "attackzonearraysize", _GUICtrlListBox_GetCount($attackzonelistGUI))
	Else
		IniWrite($sIni, $savedname, "attackzonearraysize", 1)
		IniWrite($sIni, $savedname, "attackzone0", "empty")
	EndIf
	IniWrite($sIni, $savedname, "safezone", GUICtrlRead($safezoneGUI))

EndFunc   ;==>Savesaved


Func Loadsaved()
	Select
		Case _GUICtrlListBox_GetCurSel($savedattackzonelistGUI) = -1
			MsgBox(0x10, "Error", "No save selected")
			Return
	EndSelect
	clearlist()
	$xSafezone = Number(StringLeft(IniRead($sIni, GUICtrlRead($savedattackzonelistGUI), "safezone", ""), 4))
	$ySafezone = Number(StringRight(IniRead($sIni, GUICtrlRead($savedattackzonelistGUI), "safezone", ""), 4))
	GUICtrlSetData($safezoneGUI, $xSafezone & ", " & $ySafezone)

	Dim $attackzonelist[IniRead($sIni, GUICtrlRead($savedattackzonelistGUI), "attackzonearraysize", 1)]
	If (IniRead($sIni, GUICtrlRead($savedattackzonelistGUI), "attackzone0", "empty") <> "empty") Then
		For $i = 0 To (UBound($attackzonelist) - 1)
			$attackzonelist[$i] = IniRead($sIni, GUICtrlRead($savedattackzonelistGUI), "attackzone" & $i, "")
			_GUICtrlListBox_AddString($attackzonelistGUI, $attackzonelist[$i])
		Next
	EndIf
EndFunc   ;==>Loadsaved

Func Addslot()
	If _GUICtrlListBox_FindString($savedattackzonelistGUI, GUICtrlRead($savednameeditGUI), True) <> -1 Then
		_GUICtrlListBox_SetCurSel($savedattackzonelistGUI, _GUICtrlListBox_FindString($savedattackzonelistGUI, GUICtrlRead($savednameeditGUI), True))
		MsgBox(0x10, "Error", "Duplicate name")
		Return
	EndIf
	Local $temp = GUICtrlRead($savednameeditGUI)
	If $temp = "" Then
		MsgBox(0, "Error", "Please create a unique name in the input box")
		Return
	EndIf
	_GUICtrlListBox_AddString($savedattackzonelistGUI, GUICtrlRead($savednameeditGUI))
	IniWrite($sIni, "savednames", "numberofslots", _GUICtrlListBox_GetCount($savedattackzonelistGUI))
	IniWrite($sIni, "savednames", _GUICtrlListBox_GetCount($savedattackzonelistGUI), GUICtrlRead($savednameeditGUI))
	GUICtrlSetData($savednameeditGUI, "")
EndFunc   ;==>Addslot

Func Deleteslot()
	If (_GUICtrlListBox_GetCurSel($savedattackzonelistGUI)) = -1 Then
		MsgBox(0x10, "Error", "No save selected.")
		Return
	EndIf
	If MsgBox(0x34, "Warning", "Are you sure you want to delete?") = 6 Then
		_GUICtrlListBox_DeleteString($savedattackzonelistGUI, _GUICtrlListBox_GetCurSel($savedattackzonelistGUI))
	EndIf
	IniDelete($savedattackzonelistGUI, GUICtrlRead($savedattackzonelistGUI))
	IniWrite($sIni, "savednames", "numberofslots", _GUICtrlListBox_GetCount($savedattackzonelistGUI))
EndFunc   ;==>Deleteslot

#EndRegion Unimportant botstart
#EndRegion Bot Functions
#Region Attack Tab

$tabattack = GUICtrlCreateTabItem("Attack Options") ; ATTACK OPTIONS ------------------------------------------------------------------------


GUICtrlCreateGroup("Type of attacking", 10, 40, 190, 100)
$radiocloserangeGUI = GUICtrlCreateRadio("Close range attacking", 25, 70, 150, 20)
GUICtrlSetTip($radiocloserangeGUI, "This will attack monsters only near you. Wait for them to come to you." & @CRLF & "This is good in areas with aggressive monsters", "Close range attacking")
GUICtrlSetOnEvent(-1, "closerange")

$radiofullrangeGUI = GUICtrlCreateRadio("Full screen attacking", 25, 90, 150, 20)
GUICtrlSetTip($radiofullrangeGUI, "This will attack any monster any monster in the screen." & @CRLF & "This is good in areas with passive monsters", "Full screen attacking")
GUICtrlSetOnEvent(-1, "fullrange")

$radiocustomrangeGUI = GUICtrlCreateRadio("Custom Range", 25, 110, 150, 20)
GUICtrlSetOnEvent(-1, "customrange")
GUICtrlCreateGroup("", -99, -99, 1, 1)

$picture = GUICtrlCreatePic("PKOscreenshot2.jpg", $boxoffsetx, $boxoffsety, 300, 225)
GUICtrlSetOnEvent(-1, "setboundaries")
GUICtrlSetState(-1, $GUI_DISABLE)


$picture = GUICtrlCreatePic("PKOscreenshot2.jpg", $boxoffsetx, $boxoffsety, 300, 225)
GUICtrlSetOnEvent(-1, "setboundaries")
$hBox = GUICtrlCreateLabel("", 1, 1, 1, 1, BitOR($GUI_SS_DEFAULT_LABEL, $SS_WHITEFRAME))

GUICtrlCreateTabItem("")

#EndRegion Attack Tab

#Region Attack Functions

Func closerange()

	Local $width = 300
	Local $height = 225
	$boxleft = ($width * 3 / 8); total length * ratio + offset
	$boxtop = ($height / 2 * 35 / 64) ; total height * ratio + offset
	$boxwidth = $width * 5 / 8 - $boxleft ; total length * ratio - position left
	$boxheight = $height * 26 / 40 - $boxtop ; total height * ratio - position top

	GUISwitch($handleGUI, $tabattack)
	GUICtrlSetPos($hBox, $boxleft + $boxoffsetx, $boxtop + $boxoffsety, $boxwidth, $boxheight)

EndFunc   ;==>closerange

Func fullrange()

	Local $width = 300
	Local $height = 225
	$boxleft = ($width * (1 / 5)) ; total length * ratio + offset
	$boxtop = ($height * (1 / 5))
	$boxwidth = ($width * (4 / 5)) - $boxleft
	$boxheight = $height * (7 / 8) - $boxtop

	GUISwitch($handleGUI, $tabattack)
	GUICtrlSetPos($hBox, $boxleft + $boxoffsetx, $boxtop + $boxoffsety, $boxwidth, $boxheight)

EndFunc   ;==>fullrange

Func customrange()
	Local $width = 300
	Local $height = 225

	GUISwitch($handleGUI, $tabattack)
	GUICtrlSetPos($hBox, $boxdim[0], $boxdim[1], $boxdim[2], $boxdim[3])
EndFunc   ;==>customrange


Func setboundaries()
	If Not (GUICtrlRead($radiocustomrangeGUI) = 1) Then Return
	Local $pos = MouseGetPos()
	$boxleft = $boxdim[0] - $boxoffsetx
	$boxtop = $boxdim[1] - $boxoffsety
	$boxwidth = $boxdim[2]
	$boxheight = $boxdim[3]

	Select
		Case ($pos[0]) < (($boxtotalwidth / 2) + $boxoffsetx) ;dealing with x before middle
			$boxwidth += (($boxleft + $boxoffsetx) - $pos[0]) ; maintain distance
			$boxleft = $pos[0] - $boxoffsetx
			$boxdim[0] = $pos[0]
			$boxdim[2] = $boxwidth
		Case ($pos[0]) > (($boxtotalwidth / 2) + $boxoffsetx) ;dealing with x after middle
			$boxwidth = (($pos[0]) - ($boxleft + $boxoffsetx))
			$boxdim[0] = $boxleft + $boxoffsetx
			$boxdim[2] = $boxwidth
	EndSelect
	Select
		Case ($pos[1]) < (($boxtotalheight / 2) + $boxoffsety) ; dealing with y above middle
			$boxheight += (($boxtop + $boxoffsety) - $pos[1]) ; maintain distance
			$boxtop = $pos[1] - $boxoffsety
			$boxdim[1] = $pos[1]
			$boxdim[3] = $boxheight

		Case ($pos[1]) > (($boxtotalheight / 2) + $boxoffsety) ; dealing with y below middle
			$boxheight = (($pos[1]) - ($boxtop + $boxoffsety))
			$boxdim[1] = $boxtop + $boxoffsety
			$boxdim[3] = $boxheight
	EndSelect
	GUISwitch($handleGUI, $tabattack)
	GUICtrlSetPos($hBox, $boxdim[0], $boxdim[1], $boxdim[2], $boxdim[3])
EndFunc   ;==>setboundaries
#EndRegion Attack Functions
#Region Healing Tab

$tabhealing = GUICtrlCreateTabItem("Healing Options") ; HEALING OPTIONS ------------------------------------------------------------------------
$auto_sitGUI = GUICtrlCreateCheckbox("Enable Auto Sit", 0, 40, 150, 30)
GUICtrlSetTip($auto_sitGUI, "This will make character sit while not engaged." & @CRLF & "Warning this can look Bottish.", "Enable Auto Sit")


GUICtrlCreateGroup("Flee Health levels", 10, 90, 200, 200)
GUICtrlCreateLabel("Retreat Health %", 15, 115, 110, 30)
$flee_healthlevelGUI = GUICtrlCreateInput("", 125, 112.5, 50, 20)
GUICtrlSetTip(-1, "Enter maximum percentage of health which will cause the character to run to the safezone." & @CRLF & "This is will be checked before an opponent is engaged.")

GUICtrlCreateLabel("Emergency Retreat Health %", 15, 115 + 30, 110, 30)
$emergency_healthlevelGUI = GUICtrlCreateInput("", 125, 110 + 50, 50, 20)
GUICtrlSetTip(-1, "Enter maximum percentage of health which will cause the character to run to the safezone." & @CRLF & "*This is the emergency level that will be checked during a fight")
;~ GUICtrlSetOnEvent($flee_healthlevelGUI, "flee_healthlevel")
GUICtrlCreateGroup("", -99, -99, 1, 1)

GUICtrlCreateTabItem("")
#EndRegion Healing Tab

#Region Healing Functions
#EndRegion Healing Functions
#Region Inventory

GUICtrlCreateTabItem("Item List")
$inventorysavelistinputGUI = GUICtrlCreateInput("", $guimaxx - 300, 70, 300, 30)
$InventorysavelistGUI = GUICtrlCreateList("", $guimaxx - 300, 100, 300, 200)
$ItemlistinputGUI = GUICtrlCreateInput("", 0, 70, 300, 30)
$ItemListGUI = GUICtrlCreateList("", 0, 100, 300, 200)

$ItemListtoInventorySavelistGUI = GUICtrlCreateButton("-->", 325, 100, 50, 30)
GUICtrlSetOnEvent(-1, "moveitemlisttoinventorysavelist")
$InventorySavelisttoItemListGUI = GUICtrlCreateButton("<--", 325, 130, 50, 30)
GUICtrlSetOnEvent(-1, "moveinventorysavelisttoitemlist")

;~ For $i = 0 to 9000
;~ 	$match = StringRegExp($sStr, "(?:\v)(?:"&$i&" -> )((\w| )*)", 1)
;~ 		If (Not @error) And ($match[0] <> "") Then
;~ 			_GUICtrlListBox_AddString($itemlistGUI,$match[0])
;~ 		EndIf
;~ Next

GUICtrlCreateTabItem("")
GUICtrlCreateTabItem("Inventory Selling")

GUICtrlCreateGroup("Seller Coordinates", $guimaxx - 480, 35, 140, 130)
$xsellercoordGUI = GUICtrlCreateInput("", $guimaxx - 475, 60, 60, 20, 0x2000)
$ysellercoordGUI = GUICtrlCreateInput("", $guimaxx - 405, 60, 60, 20, 0x2000)
GUICtrlCreateButton("V", $guimaxx - 415, 90, 20, 20)
GUICtrlSetOnEvent(-1, "manuallyaddsellercoordinates")
$sellercoordsGUI = GUICtrlCreateLabel("", $guimaxx - 440, 110, 80, 20)
GUICtrlCreateButton("Add Current Position", $guimaxx - 475, 130, 130, 30)
GUICtrlSetOnEvent(-1, "addcurrentpositionsellercoordinates")
GUICtrlCreateGroup("", -99, -99, 1, 1)

;~ GUICtrlCreateLabel("Starting inventory slot to sell items", 0, 100, 200, 20)
;~ $startinventorytobesoldGUI = GUICtrlCreateInput("9", 0, 120, 30, 20)
$sellinventoryGUI = GUICtrlCreateCheckbox("Enable Inventory Selling", 0, 40, 180, 30)


;~ $inventorygraphiclegendGUI = GUICtrlCreateGraphic($guimaxx - 300, 45, 38, 71)
;~ GUICtrlSetBkColor(-1, 0xffffff)
;~ GUICtrlSetColor(-1, 0)
;~ GUICtrlSetGraphic($inventorygraphiclegendGUI, $GUI_GR_COLOR, 0xC6C3C6, 0xC6C3C6)
;~ GUICtrlSetGraphic(-1, $GUI_GR_RECT, 5, 5, 28, 28)
;~ GUICtrlSetGraphic($inventorygraphiclegendGUI, $GUI_GR_COLOR, 0x00FF00, 0x00FF00)
;~ GUICtrlSetGraphic(-1, $GUI_GR_RECT, 5, 38, 28, 28)

;~ GUICtrlCreateLabel("Saved Items", $guimaxx - 262, 54, 100, 20)
;~ GUICtrlSetTip(-1, "Items not to be sold.")
;~ GUICtrlCreateLabel("Sellable Items", $guimaxx - 262, 90, 100, 20)
;~ GUICtrlSetTip(-1, "Items to be sold.")

GUICtrlCreateLabel("Inventory Selling Selection", $guimaxx - 160, 25, 163, 18)
$inventorygraphicGUI = GUICtrlCreateGraphic($guimaxx - 160, 40, 160, 304)
GUICtrlSetBkColor(-1, 0xffffff)
GUICtrlSetColor(-1, 0)

For $i = 0 To 7
	For $o = 0 To 3
		GUICtrlSetGraphic(-1, $GUI_GR_RECT, 0 + ($o * 40), 0 + ($i * 38), 40, 38)
	Next
Next
GUICtrlSetState($inventorygraphicGUI, $GUI_DISABLE)

For $i = 0 To 7
	For $o = 0 To 3
		$inventorybuttonsGUI[($i * 4) + ($o)] = GUICtrlCreateButton("Blank", $guimaxx - 160 + 1 + ($o * 40), 40 + 1 + ($i * 38), 38, 36)
		GUICtrlSetOnEvent($inventorybuttonsGUI[($i * 4) + ($o)], "inventorysellplan")
	Next
Next


GUICtrlCreateLabel("Click where you would like the inventory to be sold starting from.", $guimaxx - 300, 40, 140, 60)
GUISetFont(20, 400, 4, "arial")
$startinventorytobesoldGUI = GUICtrlCreateLabel("", $guimaxx - 200, 78, 50, 50)
GUISetFont(8.5, 400, 0)
;~ $inventorygraphic2GUI = GUICtrlCreateGraphic($guimaxx-160,45,152,304)
;~ GUICtrlSetOnEvent(-1,"inventorysellplan")


GUICtrlCreateTabItem("")
#EndRegion Inventory
#Region Inventory Functions
Func inventorysellplan()
	Local $zeroed[2]
	$coords = MouseGetPos()
	$zeroed[0] = $coords[0] - ($guimaxx - 160)
	$zeroed[1] = $coords[1] - (45)
	Local $row = Round(($zeroed[1] / 38) + .499999999999, 0) ;this will effectively find the row number of the selecteditem
	Local $column = Round(($zeroed[0] / 38) + .4999999999999, 0) ;this will effectively find the column number of the selected item
	Local $number = (($row - 1) * 4) + ($column)
	Local $count = 0
	

;~ GUICtrlSetBkColor($inventorygraphicGUI, 0xffffff)
;~ GUICtrlSetColor($inventorygraphicGUI, 0)
;~ For $i = 0 to 7
;~ 	For $o = 0 to 3
;~ 		GUICtrlSetGraphic($inventorygraphicGUI,$GUI_GR_RECT,0+($o*38),0+($i*38),38,38)
;~ 	Next
;~ Next
;~ GUICtrlDelete($inventorygraphic2GUI)
;~ 	Global $inventorygraphic2GUI = GUICtrlCreateGraphic($guimaxx - 160, 45, 152, 304)
;~ 	Global $inventorygraphic2GUI = GUICtrlCreatelabel("check",$guimaxx - 160, 45, 100, 104)


;~ 	GUICtrlSetColor($inventorygraphic2GUI, 0)
;~ 	For $i = 0 To 7
;~ 		For $o = 0 To 3
;~ 			If $count < $number Then
;~ 				GUICtrlSetGraphic($inventorygraphic2GUI, $GUI_GR_COLOR, 0xC6C3C6, 0xC6C3C6)
;~ 				GUICtrlSetGraphic($inventorygraphic2GUI, $GUI_GR_RECT, 5 + ($o * 38), 5 + ($i * 38), 28, 28)
;~ 			Else
;~ 				GUICtrlSetGraphic($inventorygraphic2GUI, $GUI_GR_COLOR, 0x00FF00, 0x00FF00)
;~ 				GUICtrlSetGraphic($inventorygraphic2GUI, $GUI_GR_RECT, 5 + ($o * 38), 5 + ($i * 38), 28, 28)
;~ 			EndIf
;~ 			$count += 1
;~ 		Next
;~ 	Next
;~
	For $i = 0 To 7
		For $o = 0 To 3
			If $count < ($number - 1) Then
				GUICtrlSetData($inventorybuttonsGUI[($i * 4) + ($o)], "Save")
			Else
				GUICtrlSetData($inventorybuttonsGUI[($i * 4) + ($o)], "Sell")
			EndIf
			$count += 1
		Next
	Next
	GUICtrlSetData($startinventorytobesoldGUI, $number)
;~ 	MsgBox(0, "", $number)
EndFunc   ;==>inventorysellplan



Func manuallyaddsellercoordinates()
	GUICtrlSetData($sellercoordsGUI, GUICtrlRead($xsellercoordGUI) & ", " & GUICtrlRead($ysellercoordGUI))
EndFunc   ;==>manuallyaddsellercoordinates

Func addcurrentpositionsellercoordinates()
	Readlocation()
	GUICtrlSetData($sellercoordsGUI, $xcoord & ", " & $ycoord)
EndFunc   ;==>addcurrentpositionsellercoordinates

Func moveitemlisttoinventorysavelist()
EndFunc   ;==>moveitemlisttoinventorysavelist

Func moveinventorysavelisttoitemlist()
EndFunc   ;==>moveinventorysavelisttoitemlist
#EndRegion Inventory Functions
#Region Failsafes Tab

GUICtrlCreateTabItem("Failsafes")

GUICtrlCreateGroup("Failsafes - Milliseconds", 10, 30, 275, 110)
GUICtrlCreateLabel("Walkto location", 15, 50, 200, 20)
$failsafewalktotimeGUI = GUICtrlCreateInput("", 215, 50, 50, 20)
GUICtrlSetTip(-1, "This is the time the bot will wait until resending the coordinates to the autowalk function in the game." & @CRLF & "It is important because sometimes the walkto funciton in the game will mess up and they need to be resent.")

GUICtrlCreateLabel("Time at Attack Zone", 15, 80, 200, 20)
$failsafeattackzoneGUI = GUICtrlCreateInput("", 215, 80, 50, 20)
GUICtrlSetTip(-1, "This is the amount of time the character will wait at an attack zone until an enemy is spotted." & @CRLF & "If this time runs up, he will move to the next attack zone.")

GUICtrlCreateLabel("Time engaged with enemy", 15, 110, 200, 20)
$failsafexpwaittimeGUI = GUICtrlCreateInput("", 215, 110, 50, 20)
GUICtrlSetTip(-1, "This is the maximum amount of time a character will attack an enemy before looking for another one." & @CRLF & "This is to prevent the bot from freezing if the program misses the check for the monster to be killed.")

GUICtrlCreateGroup("", -99, -99, 1, 1)

GUICtrlCreateTabItem("")
#EndRegion Failsafes Tab
GUISetState(@SW_SHOW, $handleGUI)

#EndRegion GUI

#Region Declare Defaults
Global $flee_healthlevel = IniRead($sIni, "defaults", "flee_healthlevel", 45)
Global $auto_sit = IniRead($sIni, "defaults", "auto_sit", 1)
Global $flee_macro = IniRead($sIni, "defaults", "flee_macro", 1)
Global $emergency_healthlevel = IniRead($sIni, "defaults", "emergency_healthlevel", 30)
Global $defaultattack = IniRead($sIni, "defaults", "attack", 1) ; 1 close ; 2 far ; 3 custom

Global $sellinventory = IniRead($sIni, "defaults", "sellinventory", 4)
Global $startinventorytobesold = IniRead($sIni, "defaults", "startinventorytobesold", 9)

Global $failsafewalktotime = IniRead($sIni, "defaults", "failsafewalktotime", 6000)
Global $failsafeattackzone = IniRead($sIni, "defaults", "failsafeattackzone", 2800)
Global $failsafexpwaittime = IniRead($sIni, "defaults", "failsafexpwaittime", 13000)


For $i = 0 To 3
	$boxdim[$i] = IniRead($sIni, "custombox", $i, 1)
Next

Global $xSafezone = Number(StringLeft(IniRead($sIni, "defaults", "safezone", ""), 4))
Global $ySafezone = Number(StringRight(IniRead($sIni, "defaults", "safezone", ""), 4))
GUICtrlSetData($safezoneGUI, $xSafezone & ", " & $ySafezone)

Global $xsellercoord = Number(StringLeft(IniRead($sIni, "defaults", "sellercoords", ""), 4))
Global $ysellercoord = Number(StringRight(IniRead($sIni, "defaults", "sellercoords", ""), 4))
GUICtrlSetData($sellercoordsGUI, $xsellercoord & ", " & $ysellercoord)


Dim $attackzonelist[IniRead($sIni, "defaults", "attackzonearraysize", 1)]

If (IniRead($sIni, "defaults", "attackzone0", "empty") <> "empty") Then
	For $i = 0 To (UBound($attackzonelist) - 1)
		$attackzonelist[$i] = IniRead($sIni, "defaults", "attackzone" & $i, "")
		_GUICtrlListBox_AddString($attackzonelistGUI, $attackzonelist[$i])
	Next
EndIf

For $i = 1 To IniRead($sIni, "savednames", "numberofslots", 0)
	_GUICtrlListBox_AddString($savedattackzonelistGUI, IniRead($sIni, "savednames", $i, "Empty"))
	GUICtrlSetData($savednameeditGUI, "")
Next

;~ For $i = 0 to 4
;~ 	IniRead
Switch $defaultattack
	Case 1
		GUICtrlSetState($radiocloserangeGUI, 1)
;~ 		GUISwitch($handleGUI, $tabattack)
;~ 		closerange()
	Case 2
		GUICtrlSetState($radiofullrangeGUI, 1)
;~ 		GUISwitch($handleGUI, $tabattack)
;~ 		fullrange()
	Case 3
		GUICtrlSetState($radiocustomrangeGUI, 1)
;~ 		GUISwitch($handleGUI, $tabattack)
;~ 		customrange()
EndSwitch


Global $xAttackzone[UBound($attackzonelist)]
Global $yAttackzone[UBound($attackzonelist)]

GUICtrlSetData($flee_healthlevelGUI, $flee_healthlevel)
GUICtrlSetData($emergency_healthlevelGUI, $emergency_healthlevel)
GUICtrlSetState($auto_sitGUI, $auto_sit)
GUICtrlSetData($failsafeattackzoneGUI, $failsafeattackzone)
GUICtrlSetData($failsafewalktotimeGUI, $failsafewalktotime)
GUICtrlSetData($failsafexpwaittimeGUI, $failsafexpwaittime)

GUICtrlSetState($sellinventoryGUI, $sellinventory)
GUICtrlSetData($startinventorytobesoldGUI, $startinventorytobesold)
#EndRegion Declare Defaults



$programstart = 0
;==============================================================================================================================================================
;==============================================================================================================================================================
;==============================================================================================================================================================
;==============================================================================================================================================================
;==============================================================================================================================================================
;==============================================================================================================================================================
;==============================================================================================================================================================


$nextzone = 0
Do
	If $programstart = 1 Then
		WinActivate($title)
		WinWaitActive($title)

		Local $failsafeattackzonecounter = 0
		Do
			Readhealth()
			If ($health / $healthmax) < ($flee_healthlevel / 100) And ($programstart = 1) Then
				healatzone()
				Sleep(1000)
				walkto($xAttackzone[0], $yAttackzone[0])
				waitwalkto($xAttackzone[0], $yAttackzone[0])
				$nextzone = 1
				characteraction()
			EndIf
			ControlSend($title_b, "", $handle, "{f3}")
			ControlSend($title_b, "", $handle, "{f3}")
			Call("enemycoloursearchscript")
			If (enemycoloursearchscript() = 1) Then Call("scanarea", $enemycoord[0], $enemycoord[1])
			Sleep(50)
			$failsafeattackzonecounter += 1
			If ($failsafeattackzonecounter) > ($failsafeattackzone / 700) And (checkengaged() <> 1) And ($programstart = 1) Then
				$failsafeattackzonecounter = 0
				Sleep(50)

				resetengage()

				Readinventory()
				If ($inventoryfull = 1) And ($sellinventory = 1) Then
					sellequipment()
				Else
					walkto($xAttackzone[$nextzone], $yAttackzone[$nextzone])
					waitwalkto($xAttackzone[$nextzone], $yAttackzone[$nextzone])
					characteraction()
				EndIf
				$nextzone += 1
				If $nextzone = UBound($attackzonelist) Then $nextzone = 0
			EndIf
		Until (checkengaged() = 1) Or ($programstart = 0)
		Call("checkxpgain")
		Sleep(50)

		ControlSend($title_b, "", $handle, "^a")
		Sleep(50)
		ControlSend($title_b, "", $handle, "^a")
	EndIf
Until GUIGetMsg() = $GUI_EVENT_CLOSE
;==============================================================================================================================================================
;==============================================================================================================================================================
;==============================================================================================================================================================
;==============================================================================================================================================================
;==============================================================================================================================================================
;==============================================================================================================================================================
;==============================================================================================================================================================

;================================================================================================================================================||Ad lib function
Func ad_lib()
EndFunc   ;==>ad_lib
;==============================================================================================================================||

Func quit()
	Exit
EndFunc   ;==>Quit

Func showarray()
	Local $msg
	Local $msgxy
	For $i = 0 To (UBound($attackzonelist) - 1)
		$msg = $msg & $attackzonelist[$i] & @CRLF
		$msgxy = $msgxy & $xAttackzone[$i] & ", " & $yAttackzone[$i] & @CRLF
	Next
	MsgBox(0, "attackzonelist", $msg)
	MsgBox(0, "attackzonelist x and y", $msgxy)
EndFunc   ;==>showarray

#Region Heal functions
Func healatzone()
	walkto($xSafezone, $ySafezone)
	waitwalkto($xSafezone, $ySafezone)
	Sleep(2500)
	ControlSend($title_b, "", $handle, "{ins}")
	Do
		Sleep(50)
		Readhealth()
	Until (($healthmax - $health) < 50) Or ($programstart = 0)
EndFunc   ;==>healatzone
#EndRegion Heal functions

#Region Enemy Engaged Functions
Func walkto($xDest, $yDest)
	Readwalktomenucoords()
	If $programstart = 1 Then
		ControlSend($title_b, "", $handle, "{shiftup}")
		For $failsafe = 0 To 10
			ControlSend($title_b, "", $handle, "!r")
			Sleep(100)
			If PixelChecksum($xcoordwalktomenu + 122, $ycoordwalktomenu + 84, $xcoordwalktomenu + 132, $ycoordwalktomenu + 85) = 2601992639 Then ; The purpose of this is to wait until the menu pops up
				MouseClick("Left", $xcoordwalktomenu + 122, $ycoordwalktomenu + 84, 1, 1)
				Send($xDest)
				Sleep(100)
				Send("{tab}")
				Sleep(100)
				Send($yDest)
				Sleep(100)
				Send("{Enter}")
				Return
			EndIf
		Next
	EndIf
EndFunc   ;==>walkto

Func waitwalkto($xDest, $yDest)
	Local $failsafewalktocounter = 0
	Do
		Sleep(50)
		ReadlocationNav()
		$failsafewalktocounter += 1
		If ($failsafewalktocounter * 50) > ($failsafewalktotime) Then
			walkto($xDest, $yDest)
			$failsafewalktocounter = 0
		EndIf
	Until (Abs($xcoordNav - $xDest) < 3) And (Abs($ycoordNav - $yDest) < 3) Or ($programstart = 0)
EndFunc   ;==>waitwalkto

Func enemycoloursearchscript()
	Local $enemyspotted = 0
	While ($enemyspotted = 0) And ($programstart = 1)
		ControlSend($title_b, "", $handle, "{shiftup}")
		ControlSend($title_b, "", $handle, "{shiftdown}")
		Sleep(5)
		For $iSwitch = 1 To 4 ;repeats until one is found or each is checked
			Switch $iSwitch
				Case 1
					$enemycoord = PixelSearch($searchxmin, $searchymin, $searchxmax, $searchymax, $enemylvllower, 0, $pixelsearchstep)
					If Not @error Then
						$enemyspotted = 1
						$iSwitch = 4
					EndIf
				Case 2
					$enemycoord = PixelSearch($searchxmin, $searchymin, $searchxmax, $searchymax, $enemylvllow, 0, $pixelsearchstep)
					If Not @error Then
						$enemyspotted = 1
						$iSwitch = 4
					EndIf
				Case 3
					$enemycoord = PixelSearch($searchxmin, $searchymin, $searchxmax, $searchymax, $enemylvlverylow, 0, $pixelsearchstep)
					If Not @error Then
						$enemyspotted = 1
						$iSwitch = 4
					EndIf
				Case 4 ; no enemy is found -- make him sit if auto-sit is enabled
;~ 					If $auto_sit = 1 Then
;~ 						ControlSend($title_b, "", $handle, "{shiftup}")
;~ 						ControlSend($title_b, "", $handle, "{ins}")
;~ 						ControlSend($title_b, "", $handle, "^a")
;~ 						ControlSend($title_b, "", $handle, "{shiftdown}")
;~ 					EndIf
					Return (0) ; Enemy not found
			EndSwitch
		Next
		Return (1) ; Enemy found
	WEnd
EndFunc   ;==>enemycoloursearchscript

Func scanarea($x, $y, $cursor = 7, $opt = 1, $radius = 10) ;$opt = 1, click, $opt = 2, return coords
	Send("{shiftup}")
	Local $rows = 5
	Local $columns = 0
	Local $speed = 1
	Local $slope ; y = mx + b ; m = rise/run
	Local $hypot = ($radius ^ 2 + $radius ^ 2) ^ (1 / 2)

	For $i = 0 To $rows
		$columns += 2
		For $u = 0 To $columns
			MouseMove(($x + ($u * $hypot) - ($i * $hypot)), ($y + ($i * $hypot)), $speed)
			Readcursorid()
			If ($cursorid = $cursor) And ($opt = 1) Then ;if the id of the cursor matches the id that we're looking for
				MouseClick("left")
				Return
			EndIf
			If ($cursorid = $cursor) And ($opt = 2) Then
				Return MouseGetPos()
			EndIf
		Next
	Next
EndFunc   ;==>scanarea

Func checkengaged()
	Sleep(200)
	Readlast()
	If $lastaction = $currentweapon Then
		$engaged = 1
		Return 1
	Else
		$engaged = 0
		Return 0
	EndIf
EndFunc   ;==>checkengaged

Func resetengage()
	$M_open = _MemoryOpen($Process)
	_MemoryWrite($M_open, "0x" & $mem_lastaction, 1869768026) ; This writes the word "Zero" into the lastaction spot
	_MemoryClose($M_open)
EndFunc   ;==>resetengage

Func checkxpgain()
	Local $failsafecounter = 0
	Local $expgain = 0
	While ($expgain = 0) And ($programstart = 1)
		$failsafecounter = $failsafecounter + 1
		Sleep(50)
		Readhealth()
		Select
			Case Readexperience() <> $experience ; if the experience level changed -- this indicates a monster has been killed
				$expgain = 1
				$experience = Readexperience()
			Case ($failsafecounter * 58) > ($failsafexpwaittime) ;failsafecounter is multiplied by 58 to compensate for the length of the cycle
				$expgain = 1
			Case ($health / $healthmax) < ($emergency_healthlevel / 100)
				$expgain = 1
			Case $programstart = 0
				$expgain = 1
		EndSelect
	WEnd
EndFunc   ;==>checkxpgain

Func characteraction()
	For $failsafe = 0 To 10
		MouseClick("Left", 240, $maxwindowy - 20, 1, 1)
		Sleep(50)
		PixelSearch(355, $maxwindowy - 218, 356, $maxwindowy - 208, $characteractioncolor) ; wait for the menu to pop up
		If Not @error Then
			For $failsafe2 = 0 To 10
				Sleep(20)
				MouseClick("Left", Random(300, 419, 1), Random(($maxwindowy - 216), ($maxwindowy - 106)), 1, 0)
				PixelSearch(355, $maxwindowy - 218, 356, $maxwindowy - 208, $characteractioncolor) ; wait for the menu to close (be clicked)
				PixelSearch(355, $maxwindowy - 218, 356, $maxwindowy - 208, $characteractioncolor, 10) ; wait for the menu to close (be clicked)
				If @error Then Return
			Next
			Return
		EndIf
	Next
	;square of actions are from (300, $maxwindowy-216, 419, $maxwindowy-106)
EndFunc   ;==>characteraction

#EndRegion Enemy Engaged Functions


#Region Inventory Functions
Func checkmenu($opt, $part1, $part2, $openclose, $sleeptime = 500, $cursor = 15)
	;$opt 1 is for mouseclicks
	;$opt 2 is for sending keystrokes
	;$opt 3 is for mouseclick except that $part1 contains both coordinates
	;$opt 4 is for incorporating the scanarea function
	Local $temp
	Readmenucount()
	$temp = $menucount
	While ($programstart = 1) ;there shouldn't be a failsafe if it involves selling items
		Switch $opt
			Case 1 ;sending mouseclicks
				MouseClick("left", $part1, $part2, 1, 1)
			Case 2 ;sending keystrokes
				Send($part1)
			Case 3
				MouseClick("left", $part1[0], $part1[1], 1, 1)
			Case 4
				scanarea($part1, $part2, 15, 1, 9) ;scanarea($x, $y, $cursor = 7, $opt = 1, $radius = 10) ;opt = 2 if not to click found coords
		EndSwitch
		Sleep($sleeptime)
		Readmenucount()
		Select
			Case $menucount > $temp ;if a window opened
				If $openclose = 1 Then ExitLoop
			Case $menucount < $temp ;if a window closed
				If $openclose = 2 Then ExitLoop
		EndSelect
	WEnd
EndFunc   ;==>checkmenu

Func sellequipment()
	Local $temp
	Local $empty = 1 ;by default all items are sold until found otherwise
	useoldticket() ;go to town

	walkto($xsellercoord, $ysellercoord) ;walk to seller
	waitwalkto($xsellercoord, $ysellercoord)
	Sleep(4000)

	checkmenu(4, $maxwindowx / 2, $maxwindowy / 2, 1, 1000) ;click on seller

	Readtrademenucoords()
	checkmenu(1, $xcoordtrademenu + 70, $ycoordtrademenu + 175, 1, 1000) ;click on trade
	Sleep(500)

	Do
		$empty = 1
		Readinventory()
		For $i = 8 To ($inventorysize - 1)
			If $inventory[$i][3] <> "Empty" Then
				$temp = selectinventoryslot($i + 1)
				MouseClickDrag("left", $temp[0], $temp[1], $xcoordinventorymenu - 100, $temp[1], 5)
				Sleep(100)
				Send("{enter}")
				$empty = 0
			EndIf
		Next
	Until ($empty = 1) Or ($programstart = 0)

	checkmenu(2, "{esc}", "", 2)
	checkmenu(2, "{esc}", "", 2)
EndFunc   ;==>sellequipment

Func useitem($propername)
	Local $temp
	Local $quantity
	Local $slotnumber
	Readinventory()
	For $i = 0 To ($inventorysize - 1)
		If $inventory[$i][3] = $propername Then
			$slotnumber = ($i + 1)
			$quantity = $inventory[$i][2]
			ExitLoop
		EndIf
	Next
;~ 	healatzone()
	Readmenucount()
	$temp = $menucount
	Send("!e")
	Sleep(100)
	While ($programstart = 1)
		Readmenucount()
		Select
			Case $temp > $menucount
				Send("!e")
				Sleep(100)
				$temp = $menucount
			Case $temp < $menucount
				ExitLoop
		EndSelect
	WEnd
	While ($programstart = 1)
		$temp = selectinventoryslot($slotnumber)
		MouseClick("left", $temp[0], $temp[1], 2, 1)
		Sleep(100)
		Readinventory()
		If $inventory[$slotnumber - 1][2] < $quantity Then ExitLoop
	WEnd
EndFunc   ;==>useitem


Func useoldticket()
	Local $temp
	Local $quantity
	Local $slotnumber
	Readinventory()
	For $i = 0 To ($inventorysize - 1)
		If $inventory[$i][3] = "Old Ticket" Then
			$slotnumber = ($i + 1)
			$quantity = $inventory[$i][2]
			ExitLoop
		EndIf
		If $i = ($inventorysize - 1) Then
			SetError(1)
			Return
		EndIf
	Next
	healatzone()

	Readmenucount()
	$temp = $menucount
	Send("!e")
	Sleep(100)
	While ($programstart = 1)
		Readmenucount()
		Select
			Case $temp > $menucount
				Send("!e")
				Sleep(100)
				$temp = $menucount
			Case $temp < $menucount
				ExitLoop
		EndSelect
	WEnd
	While ($programstart = 1)
		$temp = selectinventoryslot($slotnumber)
		MouseClick("left", $temp[0], $temp[1], 2, 1)
		Sleep(100)
		Readinventory()
		If $inventory[$slotnumber - 1][2] < $quantity Then ExitLoop
	WEnd
EndFunc   ;==>useoldticket


Func selectinventoryslot($number)
	WinActivate($title)
	WinWaitActive($title)
	Local $row = Round(($number / 4) + .49, 0) ;this will effectively find the row number of the selecteditem
	Local $column = (($number / 4) - Int(($number / 4) - .001)) * 4 ;this will effectively find the column number of the selected item
	Local $difference
	Readpointers()
	Readinventorymenucoords()
	MouseClick("left", ($xcoordinventorymenu + 190), ($ycoordinventorymenu + 372), 1, 1)
	MouseClick("left", ($xcoordinventorymenu + 190), ($ycoordinventorymenu + 216), 1, 1)
	Sleep(500)
	Readinventorymenuscrollposition()
	While ($programstart = 1)
		Readinventorymenuscrollposition()
		Select
			Case ($row < (($inventorymenuscrollposition / 4) - 3)) ;the selected item is above the current inventory scroll position
				MouseClick("left", ($xcoordinventorymenu + 190), ($ycoordinventorymenu + 216), 1, 1) ; push button scroll up
			Case ($row > ($inventorymenuscrollposition / 4)) ; the selected item is below the current inventory scroll position
				MouseClick("left", ($xcoordinventorymenu + 190), ($ycoordinventorymenu + 372), 1, 1) ; push button scroll down
			Case Else ; the selected item is in the view of the inventory
				$difference = (($inventorymenuscrollposition / 4) - $row) ; the difference from the point wanted and the current scroll position
				MouseMove(($xcoordinventorymenu + 50) + (36 * ($column - 1)), ($ycoordinventorymenu + 333 - (36 * $difference)))
				Return MouseGetPos()
		EndSelect
	WEnd
EndFunc   ;==>selectinventoryslot

Func Readinventory()
	$inventoryfull = 1 ;reset the inventory full check, by default it is full until it finds an empty slot
	$M_open = _MemoryOpen($Process)
	$inventorysize = _MemoryRead($M_open, (_MemoryRead($M_open, "0x" & ($mem_basePTRinventory), 'ptr') + ("0x" & $mem_offsetinventorysize)), 'byte')
	For $i = 0 To ($inventorysize - 1)
		$inventory[$i][0] = "0x" & Hex(_MemoryRead($M_open, ($mem_PTRinventory + ($i * 4)))) ; pointer to item

		$inventory[$i][1] = _MemoryRead($M_open, ($inventory[$i][0] + ("0x" & $mem_offsetinventory)), 'ushort') ; item type, raw
		$inventory[$i][2] = _MemoryRead($M_open, ($inventory[$i][0] + ("0x" & $mem_offsetinventory) + 0x2), 'ushort') ; quantity

;~ 		$match = StringRegExp($sStr, "(?:\v)(?:" & $inventory[$i][1] & " -> )(.*)(?:\v)", 1)
		$match = StringRegExp($sStr, "(?:\v)(?:" & $inventory[$i][1] & " -> )((\w| )*)", 1)
		If Not @error Then
			$inventory[$i][3] = $match[0] ;return of stringregexp
		Else
			$inventoryfull = 0
			$inventory[$i][3] = "Empty"
		EndIf
	Next
	GUISetState(@SW_SHOW, $handleGUIchild)
	WinMove("Inventory", "", @DesktopWidth - 310, 50, 300, $inventorysize * 17)
	GUICtrlSetPos($inventorylistGUI, 0, 0, 300, $inventorysize * 17)

	For $i = 0 To ($inventorysize - 1)
		_GUICtrlListBox_ReplaceString($inventorylistGUI, $i, $inventory[$i][2] & " " & $inventory[$i][3])
	Next


	_MemoryClose($M_open)
EndFunc   ;==>Readinventory
#EndRegion Inventory Functions

#Region read/write memory
Func _MemoryOpen($iv_Pid, $iv_DesiredAccess = 0x1F0FFF, $if_InheritHandle = 1)

	If Not ProcessExists($iv_Pid) Then
;~ 		MsgBox(0, "value of lastaction", "error3")
		SetError(1)
		Return 0
	EndIf

	Local $ah_Handle[2] = [DllOpen('kernel32.dll')]
;~ 	Local $ah_Handle = DllOpen('kernel32.dll') 	-the brackets are because an array is being declared, no brackets neccessary if it was a standalone variable
	If @error Then
		MsgBox(0, "value of lastaction", "error4")
		SetError(2)
		Return 0
	EndIf
	Local $av_OpenProcess = DllCall($ah_Handle[0], 'int', 'OpenProcess', 'int', $iv_DesiredAccess, 'int', $if_InheritHandle, 'int', $iv_Pid)

	If @error Then
		MsgBox(0, "value of lastaction", "error5")
		DllClose($ah_Handle[0])
		SetError(3)
		Return 0
	EndIf

	$ah_Handle[1] = $av_OpenProcess[0]
	Return $ah_Handle

EndFunc   ;==>_MemoryOpen


Func _MemoryRead($ah_Handle, $iv_Address, $sv_Type = 'dword') ; dword is 4 bytes

	If Not IsArray($ah_Handle) Then
		SetError(1)
		Return 0
	EndIf

	Local $v_Buffer = DllStructCreate($sv_Type) ;dword - maybe later I will try changing to 'char[4]' so that it will be easier to program

	If @error Then
		SetError(@error + 1)
		Return 0
	EndIf

	DllCall($ah_Handle[0], 'int', 'ReadProcessMemory', 'int', $ah_Handle[1], 'int', $iv_Address, 'ptr', DllStructGetPtr($v_Buffer), 'int', DllStructGetSize($v_Buffer), 'int', '')
	If Not @error Then
		Local $v_Value = DllStructGetData($v_Buffer, 1)
		Return $v_Value
	Else
		SetError(6)
		Return 0
	EndIf

EndFunc   ;==>_MemoryRead


Func _MemoryWrite($ah_Handle, $iv_Address, $v_Data, $sv_Type = 'dword')

	If Not IsArray($ah_Handle) Then
		SetError(1)
		Return 0
	EndIf

	Local $v_Buffer = DllStructCreate($sv_Type)

	If @error Then
		SetError(@error + 1)
		Return 0
	Else
		DllStructSetData($v_Buffer, 1, $v_Data)
		If @error Then
			SetError(6)
			Return 0
		EndIf
	EndIf

	DllCall($ah_Handle[0], 'int', 'WriteProcessMemory', 'int', $ah_Handle[1], 'int', $iv_Address, 'ptr', DllStructGetPtr($v_Buffer), 'int', DllStructGetSize($v_Buffer), 'int', '')

	If Not @error Then
		Return 1
	Else
		SetError(7)
		Return 0
	EndIf

EndFunc   ;==>_MemoryWrite


Func _MemoryClose($ah_Handle)

	If Not IsArray($ah_Handle) Then
		SetError(1)
		Return 0
	EndIf

	DllCall($ah_Handle[0], 'int', 'CloseHandle', 'int', $ah_Handle[1])
	If Not @error Then
		DllClose($ah_Handle[0])
		Return 1
	Else
		DllClose($ah_Handle[0])
		SetError(2)
		Return 0
	EndIf

EndFunc   ;==>_MemoryClose

Func Readlast()
	$M_open = _MemoryOpen($Process)
	$lastaction = _MemoryRead($M_open, "0x" & $mem_lastaction)
	_MemoryClose($M_open)
EndFunc   ;==>Readlast

Func Readcursorid()
	$M_open = _MemoryOpen($Process)
	$cursorid = _MemoryRead($M_open, "0x" & $mem_cursorid, 'byte')
	_MemoryClose($M_open)
EndFunc   ;==>Readcursorid

Func Readhealth()
	$M_open = _MemoryOpen($Process)
	$health = _MemoryRead($M_open, "0x" & $mem_health)
	$healthmax = _MemoryRead($M_open, "0x" & $mem_healthmax)
	_MemoryClose($M_open)
EndFunc   ;==>Readhealth

Func Readlocation()
	$M_open = _MemoryOpen($Process)
	$xcoord = Int(_MemoryRead($M_open, "0x" & $mem_xcoord, 'float'))
	$ycoord = Int(_MemoryRead($M_open, "0x" & $mem_ycoord, 'float'))
	_MemoryClose($M_open)
EndFunc   ;==>Readlocation

Func ReadlocationNav()
	$M_open = _MemoryOpen($Process)
	$xcoordNav = _MemoryRead($M_open, "0x" & $mem_xcoordNav, 'ushort')
	$ycoordNav = _MemoryRead($M_open, "0x" & $mem_ycoordNav, 'ushort')
	_MemoryClose($M_open)
EndFunc   ;==>ReadlocationNav

Func Readblinkercursor()
	$M_open = _MemoryOpen($Process)
	$xcoordblinkercursor = _MemoryRead($M_open, "0x" & $mem_xcoordblinkercursor, 'ushort')
	$ycoordblinkercursor = _MemoryRead($M_open, "0x" & $mem_ycoordblinkercursor, 'ushort')
	_MemoryClose($M_open)
EndFunc   ;==>Readblinkercursor

Func Readinventorymenucoords()
	$M_open = _MemoryOpen($Process)
	$xcoordinventorymenu = _MemoryRead($M_open, "0x" & $mem_xcoordinventorymenu, 'ushort')
	$ycoordinventorymenu = _MemoryRead($M_open, "0x" & $mem_ycoordinventorymenu, 'ushort')
;~ 	MsgBox(0,"",$xcoordinventorymenu)
	_MemoryClose($M_open)
EndFunc   ;==>Readinventorymenucoords
;button push inventory up ($xcoordinventorymenu + 159), ($ycoordinventorymenu-138)
;button push inventory down ($xcoordinventorymenu + 159), ($ycoordinventorymenu+19)
;inventory slots are all 36x36 spread apart... starting spot is. ($xcoordinventorymenu + 18), ($ycoordinventorymenu-130)

Func Readwalktomenucoords()
	$M_open = _MemoryOpen($Process)
	$xcoordwalktomenu = _MemoryRead($M_open, "0x" & $mem_xcoordwalktomenu, 'ushort')
	$ycoordwalktomenu = _MemoryRead($M_open, "0x" & $mem_ycoordwalktomenu, 'ushort')
	_MemoryClose($M_open)
EndFunc   ;==>Readwalktomenucoords

Func Readtrademenucoords()
	$M_open = _MemoryOpen($Process)
	$xcoordtrademenu = _MemoryRead($M_open, "0x" & $mem_xcoordtrademenu, 'ushort')
	$ycoordtrademenu = _MemoryRead($M_open, "0x" & $mem_ycoordtrademenu, 'ushort')
	_MemoryClose($M_open)
EndFunc   ;==>Readtrademenucoords

Func Readinventorymenuscrollposition()
	$M_open = _MemoryOpen($Process)
	$inventorymenuscrollposition = _MemoryRead($M_open, "0x" & $mem_inventorymenuscrollposition, 'ushort')
	_MemoryClose($M_open)
EndFunc   ;==>Readinventorymenuscrollposition

Func Readscreenresolution()
	$M_open = _MemoryOpen($Process)
	$screenresolution = _MemoryRead($M_open, "0x" & $mem_screenresolution, 'ushort')
	_MemoryClose($M_open)
	Select
		Case $screenresolution = 1024
			$maxwindowx = 1024
			$maxwindowy = 768
		Case $screenresolution = 800
			$maxwindowx = 800
			$maxwindowy = 600
	EndSelect
EndFunc   ;==>Readscreenresolution

Func Readexperience()
	$M_open = _MemoryOpen($Process)
	Local $temp = _MemoryRead($M_open, "0x" & $mem_experience)
	_MemoryClose($M_open)
	Return $temp
EndFunc   ;==>Readexperience

Func Readmenucount()
	$M_open = _MemoryOpen($Process)
	$menucount = _MemoryRead($M_open, "0x" & $mem_menucount, 'ushort')
	_MemoryClose($M_open)
EndFunc   ;==>Readmenucount

Func Readpointers()
	$M_open = _MemoryOpen($Process)
	$mem_health = String(Hex(("0x" & $mem_offsethealth) + _MemoryRead($M_open, "0x" & $mem_PTRhealth)))
	$mem_healthmax = String(Hex(("0x" & $mem_offsethealthmax) + _MemoryRead($M_open, "0x" & $mem_PTRhealth)))
	$mem_experience = String(Hex(("0x" & $mem_offsetexperience) + _MemoryRead($M_open, "0x" & $mem_PTRexperience)))
	$mem_PTRinventory = _MemoryRead($M_open, _MemoryRead($M_open, ("0x" & $mem_basePTRinventory), 'ptr') + ("0x" & $mem_offsetPTRinventory) + (4), 'ptr')
	$mem_xcoordinventorymenu = String(Hex(("0x" & "28") + _MemoryRead($M_open, _MemoryRead($M_open, ("0x" & $mem_PTRcoordsinventorymenu), 'ptr') + ("0x" & $mem_offsetcoordsinventorymenu))))
	$mem_ycoordinventorymenu = String(Hex(("0x" & "4c") + _MemoryRead($M_open, _MemoryRead($M_open, ("0x" & $mem_PTRcoordsinventorymenu), 'ptr') + ("0x" & $mem_offsetcoordsinventorymenu))))
	$mem_xcoordwalktomenu = String(Hex(("0x" & "28") + _MemoryRead($M_open, _MemoryRead($M_open, ("0x" & $mem_PTRcoordswalktomenu), 'ptr') + ("0x" & $mem_offsetcoordswalktomenu))))
	$mem_ycoordwalktomenu = String(Hex(("0x" & "4c") + _MemoryRead($M_open, _MemoryRead($M_open, ("0x" & $mem_PTRcoordswalktomenu), 'ptr') + ("0x" & $mem_offsetcoordswalktomenu))))
	$mem_xcoordtrademenu = String(Hex(("0x" & "28") + _MemoryRead($M_open, _MemoryRead($M_open, ("0x" & $mem_PTRcoordstrademenu), 'ptr') + ("0x" & $mem_offsetcoordstrademenu))))
	$mem_ycoordtrademenu = String(Hex(("0x" & "4c") + _MemoryRead($M_open, _MemoryRead($M_open, ("0x" & $mem_PTRcoordstrademenu), 'ptr') + ("0x" & $mem_offsetcoordstrademenu))))
	_MemoryClose($M_open)
EndFunc   ;==>Readpointers

Func Readweapon()
	$M_open = _MemoryOpen($Process)
	Local $weapon = _MemoryRead($M_open, "0x" & $mem_weapon, 'ubyte')
	_MemoryClose($M_open)
	Switch $weapon
		Case 185
			$currentweapon = $Dual_Weapon

		Case 141
			$currentweapon = $Greatsword

		Case 146
			$currentweapon = $Bow

		Case 151
			$currentweapon = $Firegun

		Case 137
			$currentweapon = $Sword

		Case 156 ;guessing aja
			$currentweapon = $Dagger

		Case 161 ;guessing aja
			$currentweapon = $Short_Staff

		Case 122
			$currentweapon = $Bare_Hand
	EndSwitch
EndFunc   ;==>Readweapon
#cs Corresponding Weapon Values
	Global $Dual_Weapon = 1818326340 	;185 *values for new way to find equipped weapon
	Global $Greatsword = 1634038343 	;141
	Global $Sword = 1919907667 			;137
	Global $Bow = 7827266 				;146
	Global $Firegun = 1701996870 		;151
	Global $Dagger = 1734828356
	Global $Short_Staff = 1919903827
	Global $Bare_Hand = 1701994818 		;122
#ce

Func Getinformation() ;This is to get new information about the new items
	Local $base = "0x08977498" ; this is subject to change often
	Local $offset = "0x364"
	Local $openfile = FileOpen("New.txt", 1)
	Dim $hugearray[4001][2] ;each has a number and a text
	For $i = 0 To 4000
;~ 	For $i = 0 to 5
		$M_open = _MemoryOpen($Process)
		$hugearray[$i][0] = _MemoryRead($M_open, ($base + ($offset * $i)), 'ushort')
;~ 	$hugearray[$i][1] = _MemoryRead($M_open,($base+($offset*$i)+0x2),'char[42]')
		$hugearray[$i][1] = _MemoryRead($M_open, ($base + ($offset * $i) + 0x4), 'char[42]')
		FileWrite($openfile, $hugearray[$i][0] & " -> " & $hugearray[$i][1] & @CRLF)
;~ 	MsgBox(0,"",$hugearray[$i][1])
;~ 	MsgBox(0,"",_MemoryRead($M_open,($base+($offset*$i)),'ushort'))
	Next

	FileClose($openfile)
	_MemoryClose($M_open)
	MsgBox(0, "complete", "Finished!")
EndFunc   ;==>Getinformation

;fail scenarios:
;"dialog box please input valid coordinates comes up"
;disconnected from server
;character dies
;character gets "stuck"
#EndRegion read/write memory

;~ Since the first question was pretty basic, I wasn't going to go into an advanced method for the second. But, since you asked, register WM_COMMAND and watch for EN_CHANGE messages from the input control as your trigger to update the selection in the listbox. If you search the forum for WM_COMMAND and EN_CHANGE, you'll find working examples.;### Tidy Error -> func is never closed in your script.;### Tidy Error -> func is never closed in your script.