#include <GUIConstants.au3>
#include <GuiConstantsEx.au3>
#include <GuiSlider.au3>
#include <GUIListBox.au3>
#include <Misc.au3>
#include <_HotKey.au3>


Opt("pixelcoordmode", 2)
Opt("mousecoordmode", 2)
Opt("GUIOnEventMode", 1)
Opt("WinTitleMatchMode", 4)
Opt("MouseClickDelay", 20)
Opt("MouseClickDownDelay", 30)

HotKeySet("s", "scanarea")
HotKeySet("t", "scanarea2")
HotKeySet("q", "scanarea3")
HotKeySet("d", "dispvar")
HotKeySet("u", "upx")
HotKeySet("j", "downx")
HotKeySet("p", "upy")
HotKeySet(";", "downy")


Global $offsetx = 13
Global $offsety = 17
Global $speed = 0
Global $multx = 6
Global $multy = 9

While 1

WEnd


Func upx()
	$multx += 1
EndFunc   ;==>upx

Func downx()
	$multx -= 1
EndFunc   ;==>downx

Func downy()
	$multy -= 1
EndFunc   ;==>downy


Func upy()
	$multy += 1
EndFunc   ;==>upy

;~ sleep(3000)
;~ $dif = TimerDiff($begin)
;~ MsgBox(0,"Time Difference",$dif)
;48
Func scanarea3()
;~ 	$begin = TimerInit()
	Local $mousexy = MouseGetPos()
	Local $x = $mousexy[0], $y = $mousexy[1]
	Local $rows = 5
	Local $columns = 0
	Local $radius = 15
	Local $slope  ; y = mx + b ; m = rise/run
	Local $hypot = ($radius ^ 2 + $radius ^ 2) ^ (1 / 2)

	For $i = 0 To $rows
		$columns += 2
		For $u = 0 To $columns
;~ 			MouseClick("left",($x+($u*$hypot)-($i*$hypot)),($y+($i*$hypot)),1,$speed)
			MouseMove(($x+($u*$hypot)-($i*$hypot)),($y+($i*$hypot)), $speed)
			MouseClick("left")
		Next
	Next
;~ 	$dif = TimerDiff($begin)
;~ 	MsgBox(0,"Time Difference",$dif)
EndFunc   ;==>scanarea3

;43
Func scanarea()
	$begin = TimerInit()
	Local $mousexy = MouseGetPos()
	Local $x = $mousexy[0], $y = $mousexy[1]
;~ 	MsgBox(0,"","")
;~ 	Select
;~ 		Case $defaultattackfull = 1
;~ 		Case $defaultattackclose = 1

	For $i = 0 To 3
;~ 		MouseMove($x + ($i * 4 * 3), $y + $i * 3 * 3, 0)
;~ 				MouseMove($x + $i * 4 * 1.5, $y + $i * 4 * 1.5 + 35, 0) 			;Remember the cartesian plane and the rise over run rule, In this case it's balik
		MouseMove(($x + $offsetx) + ($i * - 2 * $multx), (($y + $offsety) + ($i * $multy)), $speed) ;this one is -2/-1

		MouseClick("left")

		MouseMove(($x + $offsetx) + ($i * - 1 * $multx), (($y + $offsety) + ($i * $multy)), $speed) ; -1/-1

		MouseClick("left")

		MouseMove(($x + $offsetx), (($y + $offsety) + ($i * $multy)), $speed) ; 0/-1

		MouseClick("left")

		MouseMove(($x + $offsetx) + ($i * 1 * $multx), (($y + $offsety) + ($i * $multy)), $speed) ; 1/-1

		MouseClick("left")

		MouseMove(($x + $offsetx) + ($i * 2 * $multx), (($y + $offsety) + ($i * $multy)), $speed) ; 2/-1

		MouseClick("left")

	Next
	For $i = 4 To 7

		MouseMove(($x + $offsetx) + ($i * - 2 * $multx), (($y + $offsety) + ($i * $multy)), $speed) ;this one is -2/-1

		MouseClick("left")
		MouseMove(($x + $offsetx) + ($i * - 1 * $multx), (($y + $offsety) + ($i * $multy)), $speed) ; -1/-1
		MouseClick("left")

		MouseMove(($x + $offsetx) + ($i * - 0.5 * $multx), (($y + $offsety) + ($i * $multy)), $speed) ; -1/-1

		MouseClick("left")

		MouseMove(($x + $offsetx), (($y + $offsety) + ($i * $multy)), $speed) ; 0/-1

		MouseClick("left")

		MouseMove(($x + $offsetx) + ($i * 0.5 * $multx), (($y + $offsety) + ($i * $multy)), $speed) ; 1/-1

		MouseClick("left")

		MouseMove(($x + $offsetx) + ($i * 1 * $multx), (($y + $offsety) + ($i * $multy)), $speed) ; 1/-1

		MouseClick("left")

		MouseMove(($x + $offsetx) + ($i * 2 * $multx), (($y + $offsety) + ($i * $multy)), $speed) ; 2/-1

		MouseClick("left")

	Next

;~ 		Case $defaultattackfull = 1
;~ 		Case $defaultattackclose = 1
;~ 			For $i = 0 To 7
;~ 				MouseMove($x + $i * 4 * 2, $y + $i * 4 * 1.5 + 35, 0)
;~
;~ 				If $cursorid = 7 Then
;~ 				ControlClick($title_b,"","","left",1)
;~ 					MouseClick("left")
;~ 					ExitLoop
;~ 				EndIf
;~ 			Next
;~ 	EndSelect
	$dif = TimerDiff($begin)
	MsgBox(0,"Time Difference",$dif)
EndFunc   ;==>scanarea

Func dispvar()
	MsgBox(0, "vars", "x" & @CRLF & $multx & @CRLF & "y" & $multy)
EndFunc   ;==>dispvar


Func scanarea2()
	$begin = TimerInit()
	Local $mousexy = MouseGetPos()

	Local $x = $mousexy[0], $y = $mousexy[1]
;~ 	MsgBox(0,"","")
;~ 	Select
;~ 		Case $defaultattackfull = 1
;~ 		Case $defaultattackclose = 1

	For $i = 0 To 3
;~ 		MouseMove($x + ($i * 4 * 3), $y + $i * 3 * 3, 0)
;~ 				MouseMove(($x + $i * 4 * 1.5, $y + $i * 4 * 1.5 + 35, 0) 			;Remember the cartesian plane and the rise over run rule, In this case it's balik
		MouseClick("left", ($x + $offsetx) + ($i * - 2 * $multx), (($y + $offsety) + ($i * $multy)), 1, $speed) ;this one is -2/-1

		MouseClick("left", ($x + $offsetx) + ($i * - 1 * $multx), (($y + $offsety) + ($i * $multy)), 1, $speed) ; -1/-1

		MouseClick("left", ($x + $offsetx), (($y + $offsety) + ($i * $multy)), 1, $speed) ; 0/-1

		MouseClick("left", ($x + $offsetx) + ($i * 1 * $multx), (($y + $offsety) + ($i * $multy)), 1, $speed) ; 1/-1

		MouseClick("left", ($x + $offsetx) + ($i * 2 * $multx), (($y + $offsety) + ($i * $multy)), 1, $speed) ; 2/-1



	Next
	For $i = 4 To 7

		MouseClick("left", ($x + $offsetx) + ($i * - 2 * $multx), (($y + $offsety) + ($i * $multy)), 1, $speed) ;this one is -2/-1

		MouseClick("left", ($x + $offsetx) + ($i * - 1 * $multx), (($y + $offsety) + ($i * $multy)), 1, $speed) ; -1/-1
		MouseClick("left", ($x + $offsetx) + ($i * - 0.5 * $multx), (($y + $offsety) + ($i * $multy)), 1, $speed) ; -1/-1

		MouseClick("left", ($x + $offsetx), (($y + $offsety) + ($i * $multy)), 1, $speed) ; 0/-1

		MouseClick("left", ($x + $offsetx) + ($i * 0.5 * $multx), (($y + $offsety) + ($i * $multy)), 1, $speed) ; 1/-1

		MouseClick("left", ($x + $offsetx) + ($i * 1 * $multx), (($y + $offsety) + ($i * $multy)), 1, $speed) ; 1/-1

		MouseClick("left", ($x + $offsetx) + ($i * 2 * $multx), (($y + $offsety) + ($i * $multy)), 1, $speed) ; 2/-1



	Next

;~ 		Case $defaultattackfull = 1
;~ 		Case $defaultattackclose = 1
;~ 			For $i = 0 To 7
;~ 				MouseMove($x + $i * 4 * 2, $y + $i * 4 * 1.5 + 35, 0)
;~
;~ 				If $cursorid = 7 Then
;~ 				ControlClick($title_b,"","","left",1)
;~ 					MouseClick("left")
;~ 					ExitLoop
;~ 				EndIf
;~ 			Next
;~ 	EndSelect
	$dif = TimerDiff($begin)
	MsgBox(0,"Time Difference",$dif)
EndFunc   ;==>scanarea2