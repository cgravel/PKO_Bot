#include <GUIConstants.au3>
#Include <Misc.au3>
Opt("pixelcoordmode", 2)
Opt("mousecoordmode", 2)
Opt("WinTitleMatchMode", 4)
$title = "Pirate"
;~ $title = "PKO Trainer"
;~ $title_b = "PKO Trainer"
$title_b = "Pirate"
$Process = WinGetProcess($title_b, "")



;~ call("debugmm", 0xCD0000)
;~ call("debugfs", 0xBD9C0A)

;0xA4C16B - colour of level 1 enemey plant
;0xFAF544 - colour of item on ground
;0xFF0000 - colour of enemy in minimap
;0x7E2914 - colour of enemy in minimap
;722, 65, 737, 80 - area to search around guy to look for enemy on minimap

;------- enemy colour list
; 0xA4C16B - small ball cactus lvl 1
; 0xF0AE6B - tall cactus with flower on top lvl 4 - if this is the value of the colour then you need to click aprox 10 pixels lower
; 0xF2F9A4 - tortoise
;------- object colour list
;0x538BA7 - colour of gray area in health bar when there is no health
;75,183
;~ #region preset variables



Global $failsafewait = 150
Global $enemylvllower = 0xD9FF77
Global $enemylvllow = 0x00FF0C 	;23 and 28 - 5 levels lower
Global $enemylvlverylow = 0xC0C0C0
Global $enemytoolowlvl = 0xC6C3C6
;~ 0xF70CF7

;~ Global $enemytextgreen = 0x00FF08	;17 and 23 - 5 levels lower

Global $enemy1 = 0xA4C16B ; lvl 1 plant in shaitan town
Global $enemy2 = 0xC0D071 ;0xF0AE6B - flower part lvl 4
Global $enemy3 = 0xF2F9A4 ;tortoise flower lvl 7
Global $enemy4 = 0xDBD6B8 ; lv8 cuddly sheep
Global $enemy5 = 0x57B9A9 ; lv9 marsh spirit
Global $enemy6 = 0xCF973B ; bear cub lvl 12
Global $enemy7 = 0xD5D5E7 ; lv17 angelic bear
;~ call("debug",$enemy7)
;~ #endregion

Global $maxrange = 60
Global $lowhealth = 40

Global $Dual_Weapon = 1818326340
Global $Greatsword = 1634038343
Global $Bow = 7827266
Global $currentweapon
Global $xpgain=542136389

Global $coord2
Global $maxwindowx = 1024
Global $maxwindowy = 768

Global $optionsset=0
Global $xcoord = 0
Global $ycoord = 0
Global $lastaction = 0

Global $mem_xcoord = "006C4EA8"
Global $mem_ycoord = "006C4EAC"
Global $mem_lastaction = "006C9790" ; previous version - 006C27F8


#region GUI
$menu1 = GUICtrlCreateMenu("Start")
;~ GUICreate("PKO Trainer", 200, 50, -1, -1)
GUICreate("PKO Trainer", 500, 200, -1, -1)
Opt("GUIOnEventMode", 1)
GUISetOnEvent($GUI_EVENT_CLOSE, "Quit")
$menu1_item1 = GUICtrlCreateMenuitem("Exit", $menu1)
GUICtrlSetOnEvent(-1, "Quit")
;~ GUICtrlCreateButton("Exit", 100, 30, 100, 20)
GUICtrlCreateButton("Exit", 350, 170, 150, 30)
GUICtrlSetOnEvent(-1, "Quit")
GUICtrlCreateButton("Display lastaction value", 350, 140, 150, 30)
GUICtrlSetOnEvent(-1, "checkengaged")
GUICtrlCreateButton("Start trainer", 350, 110, 150, 30)
GUICtrlSetOnEvent(-1, "startloop")
GUICtrlCreateButton("Set options", 430, 10, 70, 30)
GUICtrlSetOnEvent(-1, "setoptions")

GUICtrlCreateLabel("Select game resolution:", 10, 10, 120, 25)
$screenresselect = GUICtrlCreateCombo("", 130, 10, 90, 25)
GUICtrlSetData($screenresselect, "1024x768|800x600")

GUICtrlCreateLabel("Select weapon:", 10, 35, 120, 25)
$weaponselect = GUICtrlCreateCombo("", 130, 35, 90, 25)
GUICtrlSetData($weaponselect, "Dual Weapons|Greatsword|Bow")
GUISetState(@SW_SHOW)
#endregion


#region Gui Functions
Func setoptions()
	Local $sr = 0
	Local $sw = 0
	Select
		Case GUICtrlRead($screenresselect) = "1024x768"
			Global $maxwindowx = 1024
			Global $maxwindowy = 768
			Local $sr = 1
		Case GUICtrlRead($screenresselect) = "800x600"
			Global $maxwindowx = 800
			Global $maxwindowy = 600
			Local $sr = 1
		Case GUICtrlRead($screenresselect) <> "1024x768" And GUICtrlRead($screenresselect) <> "800x600"
			MsgBox(0, "Error", "Please select a valid screen resolution")
	EndSelect
	Select
		Case GUICtrlRead($weaponselect) = "Dual Weapons"
			Global $currentweapon = $Dual_Weapon
			Local $sw = 1
		Case GUICtrlRead($weaponselect) = "Greatsword"
			Global $currentweapon = $Greatsword
			Local $sw = 1
		Case GUICtrlRead($weaponselect) = "Bow"
			Global $currentweapon = $Bow
			Local $sw = 1
		Case GUICtrlRead($weaponselect) <> "Dual Weapons" And GUICtrlRead($weaponselect) <> "Greatsword" And GUICtrlRead($weaponselect) <> "Bow"
			MsgBox(0, "Error", "Please select a valid weapon selection")
	EndSelect
	If $sr = 1 And $sw = 1 Then 
		$optionsset = 1
		msgbox(0,"Alert","Options are set for Weapons and Screen resolution.")
	EndIf
EndFunc   ;==>setoptions

;~ guictrlread($comboid) = "1024x768" guictrlread($comboid) = "800x600"

;~ (11, 27)

Func getcoord()
	Local $loop = 0
	While $loop = 0
		If _IsPressed("01") Then
			$mousepos = MouseGetPos()
			ControlSend("Untitled", "mouseposition", 15, @CRLF & "(" & $mousepos[0] & ", " & $mousepos[1] & ")")
			$loop = 1
		EndIf
		Sleep(250)
	WEnd
EndFunc   ;==>getcoord

#endregion

;~ HotKeySet("{Insert}", "getcoord")
HotKeySet("{end}", "stoploop")

$programstart = 0
;==============================================================================================================================================================
$loop = 0
While $loop = 0
;~ 	$Process = WinGetProcess($title_b, "")
;~ 	If WinExists("Pirate") Then
;~ 		$title = WinGetTitle("Pirate")
;~ 		WinSetTitle($title, "", $title_b)
;~ 	EndIf
;~ call("programloop")
;~ 	MsgBox(0,"title","loop is working")
	If $programstart = 1 Then
		WinActivate("Pirate King")
		WinWaitActive("Pirate King")
		Call("checkandheal")
		Call("enemycoloursearchscript1")
		Call("checkxpgain")
		Sleep(500)
		Send("^a")
		Send("^a")
;~ 	 	Sleep(500)
	EndIf
WEnd
;==============================================================================================================================================================

Func Quit()
;~ 	If WinExists($title_b) Then WinSetTitle($title_b, "", $title)
	Exit
EndFunc   ;==>Quit

Func checkandheal()
	$coord2 = PixelSearch(75, 26, 183, 31, 0x538BA7)
	If Not @error Then
		$healthlvl= (($coord2[0] - 75) / (183 - 75)) * 100
;~ 		MsgBox(0,"location","the x-location of the found pixelis "& $coord[0])
		If $healthlvl < $lowhealth Then
			Send("{INS}")
			Local $h = 0
			While $h = 0
				Global $coord2 = PixelSearch(75, 26, 183, 31, 0x538BA7)
				If Not @error Then
					$x1 = $coord2[0]
					$healthlvl1= (($x1 - 75) / (183 - 75)) * 100
				EndIf
				$coord2 = PixelSearch(75, 26, 183, 31, 0x538BA7)
				If @error Then
					$h = 1
					Send("{INS}")
				Else ;newly added
;~ 				EndIf
					Sleep(10) ;important, it's for checking decrease in health (before and after)
;~ 				$coord2 = PixelSearch(75, 26, 183, 31, 0x538BA7)
;~ 				if not @error then
					$x2 = $coord2[0]
					$healthlvl2= (($x2 - 75) / (183 - 75)) * 100	;	here!!!!
					If $healthlvl2 < $healthlvl1 Then
						Call("disengagesequence")
					EndIf
				EndIf
;~ 				msgbox(0,"made it","here1")
			WEnd
;~ 			msgbox(0,"made it","here2")
		EndIf
;~ 		msgbox(0,"made it","here3")
	EndIf
;~ 	msgbox(0,"made it","here4")
EndFunc   ;==>checkandheal

Func enemycoloursearchscript1()
;~ 	MouseMove(500, 400, 0)
	Send("{shiftdown}")
	Local $if = 0
	Local $s = 0
;~ 	Call("checkengaged")
;~ 	If Not @error Then $s = 1
	While $s = 0
		MouseMove(500, 400, 0)
		Sleep(50)
		$coord = PixelSearch(1, 1, $maxwindowx - 20, $maxwindowy - 44, $enemylvllower)
;~ 		$coord = PixelSearch(1, 1, 790, 560, $enemylvllower) ;800x600
		If Not @error Then
			$s = 1
			$if = 1
		EndIf
		If $if = 0 Then
			$coord = PixelSearch(1, 1, $maxwindowx - 20, $maxwindowy - 44, $enemylvllow)
;~ 			$coord = PixelSearch(1, 1, 790, 560, $enemylvllow) ;800x600
			If Not @error Then
				$s = 1
				$if = 1
			EndIf
		EndIf
		If $if = 0 Then
			$coord = PixelSearch(1, 1, $maxwindowx - 20, $maxwindowy - 44, $enemylvlverylow)
;~ 			$coord = PixelSearch(1, 1, 790, 560, $enemylvlverylow) ;800x600
			If Not @error Then
				$s = 1
				$if = 1
			EndIf
		EndIf
		If $s = 1 Then
;~ 			msgbox(0,"search is finding something still","coord[0] and coord[1] are "&$coord[0]&" "&$coord[1])
			Sleep(100)
			Send("{f3}")
			Send("{f3}")
			Sleep(250)
			If $coord[0] > $maxwindowx - 100 Then $coord[0] = $maxwindowx - 100
			If $coord[0] < 10 Then $coord[0] = 10
			If $coord[1] < 10 Then $coord[1] = 10
			If $coord[1] > $maxwindowy - 90 Then $coord[1] = $maxwindowy - 90
			MouseClick("left", $coord[0] + 50, $coord[1] + 40, 1, 0)
			Call("checkengaged")
			If @error Then
;~ 				msgbox(0,"whereabouts","continuing search because of error from checkengage value of currentweapon is "&$currentweapon)
				Send("{shiftdown}")
				$s = 0
				$if = 0
			Else
				Send("{shiftup}")
			EndIf
		EndIf
	WEnd
;~ 	msgbox(0,"whereabouts","finished engaging loop")
EndFunc   ;==>enemycoloursearchscript1

Func startloop()
	If $optionsset = 0 Then
		MsgBox(0, "Error", "Set options first")
	Else
		$programstart = 1
	EndIf
EndFunc   ;==>startloop

Func stoploop()
	$programstart = 0
EndFunc   ;==>stoploop

Func checkengaged()
	Readlast()
	If $lastaction <> $currentweapon Then 	; value for greatsword
;~ 			msgbox(0,"whereabouts","checkengage loop with an error")
		SetError(1)
	EndIf
;~ 	msgbox(0,"whereabouts","finished check engage loop")
EndFunc   ;==>checkengaged

Func resetengage()
	$M_open = _MemoryOpen($Process)
;~ 	msgbox(0,"value of $M_open is", $M_open[0])
	_MemoryWrite($M_open, "0x" & $mem_lastaction, "0x" & Hex(538976288))
	_MemoryClose($M_open)
EndFunc   ;==>resetengage

Func checkxpgain()
;~ 	msgbox(0,"whereabouts","entered xp search loop")
;~ 	Local $c=0
	Local $w = 0
	While $w = 0
		Readlast()
		If $lastaction = 542136389 Then $w = 1
;~ 		$c=$c+1
;~ 		if $c>40 then $w=1
;~ 		sleep(250)
;~ 	msgbox(0,"whereabouts","$lastaction =")
	WEnd
;~ 	msgbox(0,"whereabouts","finished xp search loop")
EndFunc   ;==>checkxpgain

Func disengagesequence()
	WinActivate("Pirate King")
	WinWaitActive("Pirate King")
	Send("{ins}")
	Sleep(250)
	MouseClick("left", $maxwindowx- ($maxwindowx * .02), $maxwindowy * .5, 5, 0)
	Sleep(3000)
	MouseClick("left", $maxwindowx- ($maxwindowx * .02), $maxwindowy * .5, 5, 0)
	Sleep(6000)
	Send("{ins}")
	Local $j = 0
	While $j = 0
		Sleep(200)
		$coord2 = PixelSearch(75, 26, 183, 31, 0x538BA7)
		If @error Then
			$j = 1
			Global $h = 1
			Send("{INS}")
			Sleep(500)
		EndIf
	WEnd
	MouseClick("left", $maxwindowx * .02, $maxwindowy * .5 - 50, 5, 0)
	Sleep(3000)
	MouseClick("left", $maxwindowx * .02, $maxwindowy * .5 - 25, 5, 0)
	Sleep(3000)
;~ 	send("{ins}")
EndFunc   ;==>disengagesequence

Func display()
	Readlast()
	MsgBox(0, "value of $lastaction is", $lastaction)
EndFunc   ;==>display

#region read/write memory
Func _MemoryOpen($iv_Pid, $iv_DesiredAccess = 0x1F0FFF, $if_InheritHandle = 1)

	If Not ProcessExists($iv_Pid) Then
		MsgBox(0, "value of lastaction", "error3")
		SetError(1)
		Return 0
	EndIf

	Local $ah_Handle[2] = [DllOpen('kernel32.dll') ]
;~ 	Local $ah_Handle = DllOpen('kernel32.dll') 	-the brackets are because an array is being declared, no brackets neccessary if it was a standalone variable
;~ msgbox(0,"value of ah_Handle[2] is", $ah_Handle[1])
	If @error Then
		MsgBox(0, "value of lastaction", "error4")
		SetError(2)
		Return 0
	EndIf
;~ msgbox(0,"value of ah_Handle[0] is", $ah_Handle[0]&"inherithandle"&$if_InheritHandle&"and the iv_pid"&$iv_Pid)
	Local $av_OpenProcess = DllCall($ah_Handle[0], 'int', 'OpenProcess', 'int', $iv_DesiredAccess, 'int', $if_InheritHandle, 'int', $iv_Pid)
;~ 	Local $av_OpenProcess = DllCall('kernel32.dll', 'int', 'OpenProcess', 'int', 0x1F0FFF(all access), 'int', 1, 'int', 4084)
;~ msgbox(0,"value of $av_openprocess is", $av_OpenProcess[0]) ;1820
;~ msgbox(0,"value of iv_pid is", $iv_Pid) ; 4084
	If @error Then
		MsgBox(0, "value of lastaction", "error5")
		DllClose($ah_Handle[0])
		SetError(3)
		Return 0
	EndIf

	$ah_Handle[1] = $av_OpenProcess[0]
;~ msgbox(0,"value of ah_Handle is", $ah_Handle)
	Return $ah_Handle

EndFunc   ;==>_MemoryOpen


Func _MemoryRead($ah_Handle, $iv_Address, $sv_Type = 'dword')

	If Not IsArray($ah_Handle) Then
		SetError(1)
		MsgBox(0, "value of lastaction", "error1")
		Return 0
	EndIf

	Local $v_Buffer = DllStructCreate($sv_Type)

	If @error Then
		MsgBox(0, "value of lastaction", "error2")
		SetError(@error + 1)
		Return 0
	EndIf

;~ 	DllCall($ah_Handle[0], 'int', 'ReadProcessMemory', 'int', $iv_Address, 'int', $ah_Handle[1], 'int', DllStructGetSize($v_Buffer), 'int', '', 'ptr', DllStructGetPtr($v_Buffer))
	DllCall($ah_Handle[0], 'int', 'ReadProcessMemory', 'int', $ah_Handle[1], 'int', $iv_Address, 'ptr', DllStructGetPtr($v_Buffer), 'int', DllStructGetSize($v_Buffer), 'int', '')
;~ msgbox(0,"value of ah_Handle[0],[1] is", $ah_Handle[0]&", "&$ah_Handle[1]);1,1788
	If Not @error Then
		Local $v_Value = DllStructGetData($v_Buffer, 1)
		Return $v_Value
	Else
		SetError(6)
		Return 0
	EndIf

EndFunc   ;==>_MemoryRead


Func _MemoryWrite($ah_Handle, $iv_Address, $v_Data, $sv_Type = 'dword')

	If Not IsArray($ah_Handle) Then
		SetError(1)
		Return 0
	EndIf

	Local $v_Buffer = DllStructCreate($sv_Type)

	If @error Then
		SetError(@error + 1)
		Return 0
	Else
		DllStructSetData($v_Buffer, 1, $v_Data)
		If @error Then
			SetError(6)
			Return 0
		EndIf
	EndIf

	DllCall($ah_Handle[0], 'int', 'WriteProcessMemory', 'int', $ah_Handle[1], 'int', $iv_Address, 'ptr', DllStructGetPtr($v_Buffer), 'int', DllStructGetSize($v_Buffer), 'int', '')

	If Not @error Then
		Return 1
	Else
		SetError(7)
		Return 0
	EndIf

EndFunc   ;==>_MemoryWrite


Func _MemoryClose($ah_Handle)

	If Not IsArray($ah_Handle) Then
		SetError(1)
		Return 0
	EndIf

	DllCall($ah_Handle[0], 'int', 'CloseHandle', 'int', $ah_Handle[1])
	If Not @error Then
		DllClose($ah_Handle[0])
		Return 1
	Else
		DllClose($ah_Handle[0])
		SetError(2)
		Return 0
	EndIf

EndFunc   ;==>_MemoryClose

Func Readlast()
	$M_open = _MemoryOpen($Process)
	$lastaction = _MemoryRead($M_open, "0x" & $mem_lastaction)
	_MemoryClose($M_open)
EndFunc   ;==>Readlast
#cs
	Func Score_p1_Write()
	If $Score_p1_freeze_c1 = 1 Then
	$Score_p1_Read = $freeze_score_p1
	$Score_p1_freeze_c1 = 0
	ConsoleWrite($freeze_score_p1 & @CR)
	Else
	$Score_p1_Read = GUICtrlRead($Input_p1_Score_edit)
	EndIf
	
	$M_open = _MemoryOpen($Process)
	;~ 	msgbox(0,"value of $M_open is", $M_open[0])
	_MemoryWrite($M_open, "0x" & $Mem_Score_p1, "0x" & Hex($Score_p1_Read))
	_MemoryClose($M_open)
	$freeze_score_p1 = $Score_p1_Read
	EndFunc   ;==>Score_p1_Write
#ce
#endregion