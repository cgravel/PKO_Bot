#include <GUIConstants.au3>
#include <Misc.au3>
#include <_HotKey.au3>
Opt("pixelcoordmode", 2)
Opt("mousecoordmode", 2)
Opt("GUIOnEventMode", 1)
Opt("WinTitleMatchMode", 4)
Opt("MouseClickDelay", 20)
Opt("MouseClickDownDelay", 30)
$title = "KOP2.0"
$title_b = "KOP2.0"
$title_c = "PKO Trainer by crisli_ve with very special thanks to Labrynth"
$Process = WinGetProcess($title_b, "")

#Region preset variables
#Region colours
Global $enemylvlsame = 0XFFFFFF
Global $enemylvllower = 0xD9FF77
Global $enemylvllow = 0x00FF0C ;23 and 28 - 5 levels lower
Global $enemylvlverylow = 0xC0C0C0
Global $enemytoolowlvl = 0xC6C3C6
;~ 0xF70CF7 purple
#EndRegion colours

#Region important memory values values
; Note that each value corresponds to a 4 character text which is reversed... convert to Hex then reverse and take the ASCII... I don't remember why it's reversed though X_x
Global $Dual_Weapon = 1818326340 		;185 *values for new way to find equipped weapon
Global $Greatsword = 1634038343 		;141
Global $Sword = 1919907667 				;137
Global $Bow = 7827266 					;146
Global $Firegun = 1701996870 			;151
Global $Dagger = 1734828356
Global $Short_Staff = 1919903827
Global $Bare_Hand = 1701994818 			;122
Global $xpgain = 542136389
;~ Global $xpgain = 1163415584
#EndRegion important memory values values

Global $sIni = @DesktopDir & "\PKO.ini"
Global $screenresolution
Global $currentweapon
Global $maxwindowx
Global $maxwindowy
Global $searchymin
Global $searchymax
Global $searchxmin
Global $searchxmax
Global $engaged
Global $handle = ControlGetHandle($title_b, "", "")
Global $stand ; Generic stand value
Global $playback = 0
Global $failsafetime = 5000

#Region memory addresses
Global $mem_weapon = "00794cbd" 				; 1 byte value, values above of the weapons
Global $mem_screenresolution = "007788f0" 		; find by searching the current resolution ex. 1024 or 800
Global $mem_xcoord = "0079a8f0" 				; find by searching the coordinates... this is a float value, y is less accurate and is always 4 bytes infront
Global $mem_ycoord = "0079a8f4"
Global $mem_lastaction = "007884a0" 			; search the text of the weapon after attacking
Global $mem_pointerhealth = "0079a72c" 			; search for health and search for a pointer
Global $mem_pointerexperience = "00792abc"
Global $offsethealth = "d8"
Global $offsethealthmax = "1c8"
Global $offsetexperience = "148"

Global $mem_cursorid = "007784ac"
;0 - 	Regular pointer cursor
;6 -    Pocket watch cursor - for game loading
;7 -	Attack cursor (sword)
;8 - 	Skill activate cursor
;9 - 	Marching cursor
;10 - 	Boat cursor to dock
;14 -	Adjust view cursor
;15 -   Dialog box cursor
;16 -   Mouse cursor that's a mouse
;20 -	Boots in a crossed circle
Global $mem_health
Global $mem_healthmax
Global $mem_experience

Global $xcoord
Global $ycoord
Global $lastaction 			; the last action that happened
Global $health 				; current health
Global $healthmax 			; max health
Global $healthlvlflee 		; this is going to be set with an option... likely to be in a percentage
Global $cursorid 			; the id of the cursor
Global $enemycoord 			; this is what has been found from the pixelsearch

Readpointers()				; Find the value of certain memory addresses by reading the pointers
Readweapon()
Readscreenresolution()
#EndRegion memory addresses
#EndRegion preset variables
Global $flee_countercheck
Global $n = 0

;~ #region button variables declared
;~ Global $flee_hlthlvl0
;~ Global $flee_checktime0
;~ Global $time_input0
;~ #endregion
#Region Hotkey List
HotKeySet("{end}", "stopbot")
#EndRegion Hotkey List
#Region iniread defaults

#Region bot iniread
Global $defaultres = IniRead($sIni, "defaults", "screenres", "no default set")
Global $defaultweapon = IniRead($sIni, "defaults", "weapon", "no default set")
#EndRegion bot iniread

#Region health iniread
Global $flee_checktime1 = IniRead($sIni, "defaults", "$flee_checktime", 1500)
Global $flee_hlthlvl1 = IniRead($sIni, "defaults", "flee_hlthlvl", 25)
Global $auto_sit1 = IniRead($sIni, "defaults", "auto_sit", 1)
Global $flee_macro1 = IniRead($sIni, "defaults", "flee_macro", 1)
#EndRegion health iniread

#Region attack iniread
Global $defaultattackclose = IniRead($sIni, "defaults", "attackclose", 1)
Global $defaultattackfull = IniRead($sIni, "defaults", "attackfull", 4)
Global $radioclose = $GUI_UNCHECKED
Global $radiofull = $GUI_UNCHECKED
#EndRegion attack iniread

#Region macro iniread
Global $time_input1 = IniRead($sIni, "defaults", "time_input1", 2000)

Dim $macro[10]
Dim $macrolist[10][2] ; 0 is name, 1 is length
readmacros()
Dim $macro_hotkeys[8] ; flee walk to and walk from will not have hotkeys
readmacrohotkeys()
#EndRegion macro iniread

#Region hotkey iniread
Dim $GUI_hotkeys[3]
readGUIhotkeys()
#EndRegion hotkey iniread

If $defaultattackfull = 1 Then
	$radiofull = $GUI_CHECKED
EndIf
If $defaultattackclose = 1 Then
	$radioclose = $GUI_CHECKED
EndIf

#EndRegion iniread defaults

#Region GUI
Global $guimaxy = 350
Global $guimaxx = 700
#Region Bot Tab
$menu1 = GUICtrlCreateMenu("Start")
GUICreate($title_c, 700, 350, -1, -1)
GUISetOnEvent($GUI_EVENT_CLOSE, "Quit")
$menu1_item1 = GUICtrlCreateMenuItem("Exit", $menu1)
GUICtrlSetOnEvent(-1, "Quit")

$tab = GUICtrlCreateTab(0, 0, 700, 30)

GUICtrlCreateTabItem("Bot")
GUICtrlCreateButton("Exit", 550, $guimaxy - 30, 150, 30)
GUICtrlSetOnEvent(-1, "Quit")
$startbot = GUICtrlCreateButton("Start botting", $guimaxx - 150, $guimaxy - 60, 150, 30)
GUICtrlSetOnEvent(-1, "startbot")
GUICtrlSetTip($startbot, "Start running the bot. " & @CRLF & "*Note: bot will not run unless the weapon and screen resolution are set", "Start botting")

;----------Drop Down Menus.        Screen Res            Weapon select
GUICtrlCreateLabel("Select game resolution:", 10, 40, 120, 25)
$screenresselect = GUICtrlCreateCombo($defaultres, 130, 35, 140, 25)
GUICtrlSetData($screenresselect, "1024x768|800x600")

GUICtrlCreateLabel("Select weapon:", 10, 65, 120, 25)
$weaponselect = GUICtrlCreateCombo($defaultweapon, 130, 60, 140, 25)
GUICtrlSetData($weaponselect, "Dagger|Short Staff|Dual Weapons|Greatsword|Sword|Firegun|Bow")
;----------

$saveoptions = GUICtrlCreateButton("Save default", 300, 35, 70, 30)
GUICtrlSetOnEvent(-1, "setoptionsasdefault")
GUICtrlSetTip($saveoptions, "Save options as default.", "Save default")

#EndRegion Bot Tab

#Region Attack Tab

GUICtrlCreateTabItem("Attack Options") ; ATTACK OPTIONS ------------------------------------------------------------------------
GUICtrlCreateGroup("Type of attacking", 10, 40, 190, 80)
$radiocloserange = GUICtrlCreateRadio("Close range attacking", 25, 70, 150, 20)
GUICtrlSetState($radiocloserange, $radioclose)
GUICtrlSetTip($radiocloserange, "This will attack monsters only near you. Wait for them to come to you." & @CRLF & "This is good in areas with aggressive monsters", "Close range attacking")

$radiofullrange = GUICtrlCreateRadio("Full screen attacking", 25, 90, 150, 20)
GUICtrlSetState($radiofullrange, $radiofull)
GUICtrlSetTip($radiofullrange, "This will attack any monster any monster in the screen." & @CRLF & "This is good in areas with passive monsters", "Full screen attacking")

#EndRegion Attack Tab

#Region Healing Tab

GUICtrlCreateTabItem("Healing Options") ; HEALING OPTIONS ------------------------------------------------------------------------
$flee_macro0 = GUICtrlCreateCheckbox("Enable FLEE Macro", $guimaxx - 275, 40, 150, 30)
GUICtrlSetOnEvent($flee_macro0, "flee_macro")
GUICtrlSetState($flee_macro0, $flee_macro1)
GUICtrlSetTip($flee_macro0, "This will check characters health periodically every X number of milliseconds and run a flee macro which you have programed in the macro section." & @CRLF & "This is a good back up healing function which is best used as a last resort.", "Enable FLEE Macro")

GUICtrlCreateLabel("Enter maximum percentage of health level which will initiate the flee sequence.", $guimaxx - 275, 100, 200, 30)
$flee_hlthlvl0 = GUICtrlCreateInput("", $guimaxx - 50, 100, 50, 30)
GUICtrlSetData($flee_hlthlvl0, $flee_hlthlvl1)
GUICtrlSetOnEvent($flee_hlthlvl0, "flee_hlthlvl")
;~ $flee_hlthlvl0 = GUICtrlCreateInput($flee_hlthlvl0, $guimaxx - 50, 100, 50, 30)
;~ GUICtrlCreateLabel("Enter a positive integer for minimum health percentage before initiating FLEE macro.", $guimaxx - 275, 100, 200, 30)


GUICtrlCreateLabel("Enter a positive integer for milliseconds between health checks", $guimaxx - 275, 70, 200, 30)
$flee_checktime0 = GUICtrlCreateInput("", $guimaxx - 50, 70, 50, 30)
GUICtrlSetData($flee_checktime0, $flee_checktime1)
GUICtrlSetOnEvent($flee_checktime0, "flee_checktime")
GUICtrlSetTip($flee_checktime0, "Amount of time, in milliseconds, for program to check health level compared to low health level required to initiate FLEE macro. Doing too often will take up cpu power.", "Set healthtimecheck")

$auto_sit0 = GUICtrlCreateCheckbox("Enable Auto Sit", 0, 40, 150, 30)
GUICtrlSetOnEvent(-1, "auto_sit")
GUICtrlSetState($auto_sit0, $auto_sit1)
GUICtrlSetTip($auto_sit0, "This will make character sit while not engaged." & @CRLF & "Warning this can look Bottish.", "Enable Auto Sit")

#EndRegion Healing Tab

#Region Macro Tab

GUICtrlCreateTabItem("Macro Options") ; MACRO OPTIONS ------------------------------------------------------------------------
GUICtrlCreateLabel("Set time adjustment for Macro playback:->", $guimaxx - 275, 130, 200, 30)
$time_input0 = GUICtrlCreateInput("Set time adjustment for Macro playback:->", $guimaxx - 50, 130, 50, 30)
GUICtrlSetOnEvent($time_input0, "timedelayset")
GUICtrlSetData($time_input0, $time_input1)
GUICtrlSetTip($time_input0, "Enter an integer which will affect, positively or negatively, the time between clicks on the mouseplayback function.", "Set time adjustment")

$buttonrecorderhelp = GUICtrlCreateButton("Macro help", 0, $guimaxy - 120, 100, 30)
GUICtrlSetOnEvent(-1, "recorder_help")
GUICtrlSetTip($buttonrecorderhelp, "Find out how to use it and what it does.", "Recorder help")

$buttonsavemacro = GUICtrlCreateButton("Macro Recorder", 0, $guimaxy - 60, 100, 30)
GUICtrlSetOnEvent(-1, "record_macro")
GUICtrlSetTip($buttonsavemacro, "Records a Macro which will be stored ini file; when bot is restarted, the macros will be reloaded.", "Record Macro")

$buttondeletemacro = GUICtrlCreateButton("Macro Delete", 0, $guimaxy - 30, 100, 30)
GUICtrlSetOnEvent(-1, "delete_macro")
GUICtrlSetTip($buttonsavemacro, "Deletes the Macro in the ini file.", "Delete Macro")

$buttonrestoremacro = GUICtrlCreateButton("Macro Player", 0, $guimaxy - 90, 100, 30)
GUICtrlSetOnEvent(-1, "playback_macro")
GUICtrlSetTip($buttonrestoremacro, "Plays a previously recorded macro.", "Macro Player")

$macroGUIlist = GUICtrlCreateList("", 0, 40, 175, 145)
GUICtrlSetOnEvent($macroGUIlist, "displaymacrohotkey")
For $i = 0 To 9
	GUICtrlSetData($macroGUIlist, $i & " " & $macrolist[$i][0] & "|")
Next

GUICtrlCreateLabel("Current Hotkey set:", 175, 40, 110, 25)
$macrohotkey = GUICtrlCreateLabel(" ", 285, 40, 80, 25)

$setmacrohotkey0 = GUICtrlCreateButton("Set Hotkey", 175, 65, 95, 25)
GUICtrlSetOnEvent(-1, "setmacrohotkey1")


$disablemacrohotkey0 = GUICtrlCreateButton("Remove Hotkey", 175, 90, 95, 25)
GUICtrlSetOnEvent(-1, "disablemacrohotkey1")

;~ $enablemacrohotkey0= GUICtrlCreateButton("Set Hotkey", 175, 65, 75, 25)
;~ GUICtrlSetOnEvent(-1, "enablemacrohotkey1")
#EndRegion Macro Tab

#Region Hotkey Tab

GUICtrlCreateTabItem("HotKeys") ; HOTKEY OPTIONS ------------------------------------------------------------------------
$hotkeyGUIlist = GUICtrlCreateList("", 0, 40, 175, 145)
GUICtrlSetOnEvent($hotkeyGUIlist, "displayguihotkey")
GUICtrlSetData($hotkeyGUIlist, "0 Start Bot|1 Stop Bot")
;~ GUICtrlSetData($hotkeyGUIlist, "0 Start Bot|1 Stop Bot|2 Bring up GUI")

GUICtrlCreateLabel("Current Hotkey set:", 175, 40, 110, 25)
$guihotkey = GUICtrlCreateLabel(" ", 285, 40, 80, 25)

$setguihotkey0 = GUICtrlCreateButton("Set Hotkey", 175, 65, 95, 25)
GUICtrlSetOnEvent(-1, "setguihotkey1")

$disableguihotkey0 = GUICtrlCreateButton("Remove Hotkey", 175, 90, 95, 25)
GUICtrlSetOnEvent(-1, "disableguihotkey1")

;~ $GUI_ENABLE Control will be enabled
;~ $GUI_DISABLE

;~ $fleeadlibtime0
;~ $flee_hlthlvl0

GUISetState(@SW_SHOW)

#EndRegion Hotkey Tab





#Region Bot Functions
Func startbot()
	Local $sr = 0
	Local $sw = 0
	Select
		Case GUICtrlRead($screenresselect) = "1024x768"
			Global $maxwindowx = 1024
			Global $maxwindowy = 768
			Local $sr = 1
		Case GUICtrlRead($screenresselect) = "800x600"
			Global $maxwindowx = 800
			Global $maxwindowy = 600
			Local $sr = 1
		Case GUICtrlRead($screenresselect) <> "1024x768" And GUICtrlRead($screenresselect) <> "800x600"
			MsgBox(0, "Error", "Please select a valid screen resolution")
	EndSelect
	Select
		Case GUICtrlRead($weaponselect) = "Dual Weapons"
			Global $currentweapon = $Dual_Weapon
			Local $sw = 1
		Case GUICtrlRead($weaponselect) = "Greatsword"
			Global $currentweapon = $Greatsword
			Local $sw = 1
		Case GUICtrlRead($weaponselect) = "Bow"
			Global $currentweapon = $Bow
			Local $sw = 1
		Case GUICtrlRead($weaponselect) = "Firegun"
			Global $currentweapon = $Firegun
			Local $sw = 1
		Case GUICtrlRead($weaponselect) = "Sword"
			Global $currentweapon = $Sword
			Local $sw = 1
		Case GUICtrlRead($weaponselect) = "Dagger"
			Global $currentweapon = $Dagger
			Local $sw = 1
		Case GUICtrlRead($weaponselect) = "Short Staff"
			Global $currentweapon = $Short_Staff
			Local $sw = 1
		Case GUICtrlRead($weaponselect) <> "Dual Weapons" And GUICtrlRead($weaponselect) <> "Greatsword" And GUICtrlRead($weaponselect) <> "Bow" And GUICtrlRead($weaponselect) <> "Firegun" And GUICtrlRead($weaponselect) <> "Sword" And GUICtrlRead($weaponselect) <> "Dagger" And GUICtrlRead($weaponselect) <> "Short Staff"
			MsgBox(0, "Error", "Please select a valid weapon selection")
	EndSelect
	Select
		Case GUICtrlRead($radiocloserange) = $GUI_CHECKED
			$searchymax = $maxwindowy * 26 / 40
			$searchymin = $maxwindowy / 2 * 35 / 64
			$searchxmin = $maxwindowx * 3 / 8
			$searchxmax = $maxwindowx * 5 / 8
		Case GUICtrlRead($radiofullrange) = $GUI_CHECKED
			$searchymin = 1
			$searchymax = $maxwindowy - 44
			$searchxmin = 1
			$searchxmax = $maxwindowx - 20
	EndSelect
	If $sr = 1 And $sw = 1 Then
		$programstart = 1
		setoptions()
;~ 		setmacro_hotkeys()
		resetengage()
		$stand = 1
;~ 		AdlibEnable("ad_lib", 200) ; might be a slight error here with regards to sitting and standing
		MsgBox(0, "Alert", "Make sure the character is standing now, if not make him stand now.")
	EndIf
EndFunc   ;==>startbot

Func setoptions()
;~ 	$fleeonoff = GUICtrlRead($fleemacro)
;~ 	$flee_checktime1 = GUICtrlRead($flee_checktime0)
	$time_input1 = GUICtrlRead($time_input0)
	$flee_hlthlvl1 = GUICtrlRead($flee_hlthlvl0)
	$defaultattackclose = GUICtrlRead($radiocloserange) ;
	$defaultattackfull = GUICtrlRead($radiofullrange) ;
EndFunc   ;==>setoptions

Func setoptionsasdefault()
;~ 	IniWrite($sIni, "defaults", "flee_hlthlvl", GUICtrlRead($flee_hlthlvl0))
;~ 	IniWrite($sIni, "defaults", "fleeonoff", GUICtrlRead($fleemacro0))
;~ 	IniWrite($sIni, "defaults", "flee_checktime", GUICtrlRead($flee_checktime0))
	IniWrite($sIni, "defaults", "weapon", GUICtrlRead($weaponselect))
	IniWrite($sIni, "defaults", "screenres", GUICtrlRead($screenresselect))

	IniWrite($sIni, "defaults", "attackclose", GUICtrlRead($radiocloserange))
	IniWrite($sIni, "defaults", "attackfull", GUICtrlRead($radiofullrange))
	MsgBox(0, "Alert", "defaults are set")
EndFunc   ;==>setoptionsasdefault


#EndRegion Bot Functions
#Region Attack Option Functions
#EndRegion Attack Option Functions
#Region Healing Option Functions
Func flee_macro()
	If GUICtrlRead($flee_macro0) = 4 Then
		GUICtrlSetState($flee_checktime0, $GUI_DISABLE)
		GUICtrlSetState($flee_hlthlvl0, $GUI_DISABLE)
		$flee_macro1 = 4
	EndIf
	If GUICtrlRead($flee_macro0) = 1 Then
		GUICtrlSetState($flee_checktime0, $GUI_ENABLE)
		GUICtrlSetState($flee_hlthlvl0, $GUI_ENABLE)
		$flee_macro1 = 1
	EndIf
	IniWrite($sIni, "defaults", "flee_macro", $flee_macro1)
EndFunc   ;==>flee_macro

Func auto_sit()
	If GUICtrlRead($auto_sit0) = 1 Then
		$auto_sit1 = 1
	Else
		$auto_sit1 = 4
	EndIf
	IniWrite($sIni, "defaults", "auto_sit", GUICtrlRead($auto_sit0))
EndFunc   ;==>auto_sit

Func flee_checktime()
	$flee_checktime1 = GUICtrlRead($flee_checktime0)
	IniWrite($sIni, "defaults", "flee_checktime", GUICtrlRead($flee_checktime0))
EndFunc   ;==>flee_checktime

Func flee_hlthlvl()
	$flee_hlthlvl1 = GUICtrlRead($flee_hlthlvl0)
	IniWrite($sIni, "defaults", "flee_hlthlvl", $flee_hlthlvl1)
EndFunc   ;==>flee_hlthlvl
#EndRegion Healing Option Functions

#Region Macro functions
Func record_macro()
	Do
		$select = InputBox("Macro Recorder", _
				"0 " & $macrolist[0][0] & @CRLF & _
				"1 " & $macrolist[1][0] & @CRLF & _
				"2 " & $macrolist[2][0] & @CRLF & _
				"3 " & $macrolist[3][0] & @CRLF & _
				"4 " & $macrolist[4][0] & @CRLF & _
				"5 " & $macrolist[5][0] & @CRLF & _
				"6 " & $macrolist[6][0] & @CRLF & _
				"7 " & $macrolist[7][0] & @CRLF & _
				"8 " & $macrolist[8][0] & @CRLF & _
				"9 " & $macrolist[9][0], "Pick the corresponding number", "", 175, 250)
		Select
			Case @error = 1
				ExitLoop
			Case $select > 9 Or $select < 0
				MsgBox(0, "Invalid number", "Please type the number corresponding to your choice")
			Case $select <= 9 And $select >= 0
				$answer = MsgBox(4, "Macro Recorder", "Click Yes to proceed with macro recording. When recording press F11 to finish.")
				If $answer = 6 Then
					$macro[$select] = macro_recorder($macrolist[$select][1])
					MsgBox(0, "Finished recording", "macro has been recorded.")
					If $select >= 2 Then
						Do
							$macrolist[$select][0] = InputBox("Save as", "What do you want to call the macro?", "", "", 250, 50)
							If $macrolist[$select][0] = "Empty" Then
								MsgBox(0, "Invalid name", "Empty is an invalid name. Please chose a valid name.")
							EndIf
						Until $macrolist[$select][0] <> "Empty"
						writemacro($macro[$select], $macrolist[$select][0], $macrolist[$select][1], $select)
						GUICtrlSetData($macroGUIlist, "")
						For $i = 0 To 9
							GUICtrlSetData($macroGUIlist, $i & " " & $macrolist[$i][0] & "|")
						Next
					Else
						writemacro($macro[$select], $macrolist[$select][0], $macrolist[$select][1], $select)
					EndIf
				EndIf
		EndSelect
	Until $select <= 9 And $select >= 0
EndFunc   ;==>record_macro

Func playback_macro()
	Do
		$select = InputBox("Macro Player", _
				"0 " & $macrolist[0][0] & @CRLF & _
				"1 " & $macrolist[1][0] & @CRLF & _
				"2 " & $macrolist[2][0] & @CRLF & _
				"3 " & $macrolist[3][0] & @CRLF & _
				"4 " & $macrolist[4][0] & @CRLF & _
				"5 " & $macrolist[5][0] & @CRLF & _
				"6 " & $macrolist[6][0] & @CRLF & _
				"7 " & $macrolist[7][0] & @CRLF & _
				"8 " & $macrolist[8][0] & @CRLF & _
				"9 " & $macrolist[9][0], "Pick the corresponding number", "", 175, 250)
		Select
			Case @error = 1
				ExitLoop
			Case $select > 9 Or $select < 0
				MsgBox(0, "Invalid number", "Please type the number corresponding to your choice")
			Case $select <= 9 And $select >= 0
				$answer = MsgBox(4, "Macro Recorder", "Click Yes to proceed with macro playback.")
				If $answer = 6 Then
					macro_player($macro[$select])
				EndIf
		EndSelect
	Until $select <= 9 And $select >= 0
EndFunc   ;==>playback_macro

Func delete_macro()
	$answer = MsgBox(4, "Delete Macro", "Are you sure you want to continue with deletion?")
	If $answer = 6 Then
		Local $str = GUICtrlRead($macroGUIlist)
		Local $n = StringLeft($str, 1)
		If StringRight($str, StringLen($str) - 2) <> "Empty" Then ;check if the macro is already empty or not
			If $n > 1 Then ;only change name if the macro is not one of the 2 flee macros, and note to delete the hotkeyset for it
				IniDelete($sIni, "macrolist", $n)
				$macrolist[$n][0] = "Empty"
				GUICtrlSetData($macroGUIlist, "")
				For $i = 0 To 9
					GUICtrlSetData($macroGUIlist, $i & " " & $macrolist[$i][0] & "|")
				Next
				If StringLeft($macro_hotkeys[$n - 2], 1) <> "+" Then ;reset hotkey and account for the addition of the shift to hotkey because of enemy colourscript where shift is held down
					HotKeySet("+" & $macro_hotkeys[$n - 2])
				EndIf
				HotKeySet($macro_hotkeys[$n - 2])
			EndIf
			IniDelete($sIni, "macrolist", $n & "length")
			$macrolist[$n][1] = 0
			IniDelete($sIni, $macrolist[$n][0] & "x")
			IniDelete($sIni, $macrolist[$n][0] & "y")
			IniDelete($sIni, $macrolist[$n][0] & "time")
			MsgBox(64, "Notice", "Macro has been deleted.")
		Else
			MsgBox(0, "Error", "Nothing to be deleted.")
		EndIf
	EndIf
EndFunc   ;==>delete_macro

Func macro_recorder(ByRef $n); $n is the length *byref will return the variable $n back to the second parameter from where it was called.
	Dim $recorderarray[1][3]
	$n = 0
	Local $oldtime = TimerInit()
	Local $pressed = 1
	Local $mr = 0
	While $mr = 0
		If _IsPressed("01") Then
			If $pressed = 0 Then
				ReDim $recorderarray[$n + 1][3]
				$time = TimerDiff($oldtime)
				Local $mousepos = MouseGetPos()
				$recorderarray[$n][0] = $mousepos[0] ; x
				$recorderarray[$n][1] = $mousepos[1] ; y
				$recorderarray[$n][2] = $time
				$oldtime = TimerInit()
				$n = $n + 1
				$pressed = 1
			EndIf
		EndIf
		If Not _IsPressed("01") Then
			$pressed = 0
		EndIf
		If _IsPressed("7A") Then
			$mr = 1
		EndIf
	WEnd
	Return $recorderarray
EndFunc   ;==>macro_recorder

Func macro_player($macroarray)
	If $stand = 0 Then
		ControlSend($title_b, "", $handle, "{ins}")
		Sleep(50)
		$stand = 1
	EndIf
	$playback = 1
	WinActivate($title_b)
	WinWaitActive($title_b)
	For $i = 0 To UBound($macroarray) - 1
		$safeclick = 0
		Local $a = 0
		Local $b = 0
		While $safeclick = 0
			MouseMove($macroarray[$i][0] + $a, $macroarray[$i][1] + $b, 0)
			Readcursorid()
			If $cursorid = 7 Then
				If $macroarray[$i][0] > $maxwindowx / 2 Then
					$a = $a - 3
				Else
					$a = $a + 3
				EndIf
				If $macroarray[$i][1] > $maxwindowy / 2 Then
					If $macroarray[$i][1] > $maxwindowy / 2 Then
						$b = $b - 3
					Else
						$b = $b + 3
					EndIf
				EndIf
			Else
				MouseClick("left")
				Sleep(10)
				MouseClick("left")
				Sleep(10)
				MouseClick("left")
				$safeclick = 1
			EndIf
		WEnd
		Sleep($macroarray[$i][2] + $time_input1)
	Next
;~ 	resetengage()
EndFunc   ;==>macro_player

Func recorder_help()
	MsgBox(64, "Help on Recorder Function", _
			"What it's used for:" & @CRLF & _
			"This function is used to get the character out of danger when he has low health" & @CRLF & _
			"currently this function will only execute after he has killed an enemy so don't" & @CRLF & _
			"expect him to run while engaged in a fight. This just sets the course for the " & @CRLF & _
			"character to run to when he's low on health so he can sit and heal.")
	MsgBox(64, "Help on Recorder Function", _
			"How to use:" & @CRLF & _
			"Click on screen from the starting point of where your character will be (take" & @CRLF & _
			"note of the coordinates) keep in mind that you should be in the botting area " & @CRLF & _
			"when you begin recording on your walk back try to get your character as close" & @CRLF & _
			"as possible to the original starting location for maximum effectiveness")
	MsgBox(64, "Help on Recorder Function", _
			"What to keep in mind:" & @CRLF & _
			"When using the recording function, don't hold the mouse button down when " & @CRLF & _
			"clicking, because this will mess up the timing of the clicks and will result" & @CRLF & _
			"in an unpredictable function. For improved performance wait until character " & @CRLF & _
			"stopped moving until you click to a new location.")
EndFunc   ;==>recorder_help

Func readmacros()
	Local $macroarray[1][3]
	$macrolist[0][0] = "*Flee Macro - walkto"
	$macrolist[1][0] = "*Flee Macro - walkfrom"
	For $i = 2 To 9
		$macrolist[$i][0] = IniRead($sIni, "macrolist", $i, "Empty") ;load names of all macros to array
	Next

	For $i = 0 To 9
		$macrolist[$i][1] = IniRead($sIni, "macrolist", $i & "length", 0) ; get the length of all the macros and store in macrolist array
	Next

	For $i = 0 To 9
		If $macrolist[$i][1] > 0 Then
			ReDim $macroarray[$macrolist[$i][1]][3]
			For $j = 0 To $macrolist[$i][1] - 1 ; macrolist[$i][1] is the length, rather than using ubound command.
				$macroarray[$j][0] = IniRead($sIni, $macrolist[$i][0] & "x", $j, "") ;load macro information to macro array
				$macroarray[$j][1] = IniRead($sIni, $macrolist[$i][0] & "y", $j, "")
				$macroarray[$j][2] = IniRead($sIni, $macrolist[$i][0] & "time", $j, "")
			Next
			$macro[$i] = $macroarray
		EndIf
	Next
EndFunc   ;==>readmacros

Func writemacro($macroarray, $name, $length, $listnum)
;~ 	MsgBox(0, "length", $length)
	IniWrite($sIni, "macrolist", $listnum, $name)
	IniWrite($sIni, "macrolist", $listnum & "length", $length)
	For $i = 0 To $length - 1
		IniWrite($sIni, $name & "x", $i, $macroarray[$i][0])
		IniWrite($sIni, $name & "y", $i, $macroarray[$i][1])
		IniWrite($sIni, $name & "time", $i, $macroarray[$i][2])
;~ 		MsgBox(0, "$name, $i and $macroarray", $name & " " & $i & " " & $macroarray[$i][0] & " " & $macroarray[$i][1] & " " & $macroarray[$i][2])
	Next
EndFunc   ;==>writemacro

Func readmacrohotkeys()
	For $var = 0 To 7
		$macro_hotkeys[$var] = IniRead($sIni, "hotkey_defaults", "macro_" & $var & "_hotkey", "no hotkey set")
		If $macro_hotkeys[$var] <> "no hotkey set" Then
			HotKeySet($macro_hotkeys[$var], "macro_hotkeys" & $var)
			If StringLeft($macro_hotkeys[$var], 1) <> "+" Then
				HotKeySet("+" & $macro_hotkeys[$var], "macro_hotkeys" & $var)
			EndIf
		EndIf
	Next
EndFunc   ;==>readmacrohotkeys

Func displaymacrohotkey()
	Local $listnum = StringLeft(GUICtrlRead($macroGUIlist), 1)
	If $listnum > 1 Then
		If $macro_hotkeys[$listnum - 2] <> "no hotkey set" Then
			GUICtrlSetData($macrohotkey, _HotKeyToString($macro_hotkeys[$listnum - 2]))
		Else
			GUICtrlSetData($macrohotkey, $macro_hotkeys[$listnum - 2])
		EndIf
	EndIf
	If $listnum <= 1 Then
		GUICtrlSetData($macrohotkey, "N/A")
	EndIf
EndFunc   ;==>displaymacrohotkey

Func setmacrohotkey1()
	Local $listnum = StringLeft(GUICtrlRead($macroGUIlist), 1)
	If $listnum > 1 Then
		$key = _ChooseHotKey()
		$macro_hotkeys[$listnum - 2] = $key
		GUICtrlSetData($macrohotkey, _HotKeyToString($macro_hotkeys[$listnum - 2]))
		IniWrite($sIni, "hotkey_defaults", "macro_" & $listnum & "_hotkey", $key)
		HotKeySet($macro_hotkeys[$listnum - 2], "macro_hotkeys" & $listnum - 2)
	EndIf
EndFunc   ;==>setmacrohotkey1

;~ Func setmacro_hotkeys()
;~ 	For $i = 0 To 7
;~ 		If $macro_hotkeys[$i] <> "no hotkey set" Then
;~ 			HotKeySet($macro_hotkeys[$i], "macro_hotkeys" & $i)
;~ 		EndIf
;~ 	Next
;~ EndFunc   ;==>setmacro_hotkeys

Func timedelayset()
	$time_input1 = GUICtrlRead($time_input0)
	IniWrite($sIni, "defaults", "time_input1", GUICtrlRead($time_input0))
	Sleep(25)
EndFunc   ;==>timedelayset

#Region macro_hotkeys
Func macro_hotkeys0()
;~ 	msgbox(0,"hotkey worked","")
	macro_player($macro[2])
EndFunc   ;==>macro_hotkeys0

Func macro_hotkeys1()
	macro_player($macro[3])
EndFunc   ;==>macro_hotkeys1

Func macro_hotkeys2()
	macro_player($macro[4])
EndFunc   ;==>macro_hotkeys2

Func macro_hotkeys3()
	macro_player($macro[5])
EndFunc   ;==>macro_hotkeys3

Func macro_hotkeys4()
	macro_player($macro[6])
EndFunc   ;==>macro_hotkeys4

Func macro_hotkeys5()
	macro_player($macro[7])
EndFunc   ;==>macro_hotkeys5

Func macro_hotkeys6()
	macro_player($macro[8])
EndFunc   ;==>macro_hotkeys6
Func macro_hotkeys7()
	macro_player($macro[9])
EndFunc   ;==>macro_hotkeys7
#EndRegion macro_hotkeys

Func disablemacrohotkey1()
	Local $listnum = StringLeft(GUICtrlRead($macroGUIlist), 1)
	If $macro_hotkeys[$listnum - 2] <> "no hotkey set" Then
		HotKeySet($macro_hotkeys[$listnum - 2])
		$macro_hotkeys[$listnum - 2] = "no hotkey set"
		GUICtrlSetData($macrohotkey, "no hotkey set")
		IniWrite($sIni, "hotkey_defaults", "macro_" & $listnum & "_hotkey", "no hotkey set")
	Else
		MsgBox(0, "Error", "No hotkey can be removed if none is set.")
	EndIf
EndFunc   ;==>disablemacrohotkey1

#EndRegion Macro functions




#Region Hotkey Functions
Func stopbot()
	$enemyspotted = 1
	$programstart = 0
	AdlibDisable()
	ControlSend($title_b, "", $handle, "{shiftup}")
	WinActivate($title_c)
	WinWaitActive($title_c)
EndFunc   ;==>stopbot

;~ Func focusgui()
;~ 	WinActivate($title_c)
;~ 	WinWaitActive($title_c)
;~ EndFunc

Func readGUIhotkeys()
	$GUI_hotkeys[0] = IniRead($sIni, "GUI hotkeys", "Start Bot", "no hotkey set") ;load macro information to macro array
	$GUI_hotkeys[1] = IniRead($sIni, "GUI hotkeys", "Stop Bot", "no hotkey set")
;~    ;$GUI_hotkeys[2] = IniRead($sIni, "GUI hotkeys", "Bring up GUI", "no hotkey set")
	If $GUI_hotkeys[0] <> "no hotkey set" Then
		HotKeySet($GUI_hotkeys[0], "startbot")
		If StringLeft($GUI_hotkeys[0], 1) <> "+" Then
			HotKeySet("+" & $GUI_hotkeys[0], "startbot")
		EndIf
	EndIf
	If $GUI_hotkeys[1] <> "no hotkey set" Then
		HotKeySet($GUI_hotkeys[1], "stopbot")
		If StringLeft($GUI_hotkeys[1], 1) <> "+" Then
			HotKeySet("+" & $GUI_hotkeys[1], "stopbot")
		EndIf
	EndIf
EndFunc   ;==>readGUIhotkeys

Func displayguihotkey()
	Local $listnum = StringLeft(GUICtrlRead($hotkeyGUIlist), 1)
	If $GUI_hotkeys[$listnum] <> "no hotkey set" Then
		GUICtrlSetData($guihotkey, _HotKeyToString($GUI_hotkeys[$listnum]))
	Else
		GUICtrlSetData($guihotkey, $GUI_hotkeys[$listnum])
	EndIf
EndFunc   ;==>displayguihotkey

Func setguihotkey1()
	Local $listnum = StringLeft(GUICtrlRead($hotkeyGUIlist), 1)
	$key = _ChooseHotKey()
	$GUI_hotkeys[$listnum] = $key
	GUICtrlSetData($guihotkey, _HotKeyToString($GUI_hotkeys[$listnum]))
	IniWrite($sIni, "GUI hotkeys", StringRight(GUICtrlRead($hotkeyGUIlist), StringLen(GUICtrlRead($hotkeyGUIlist)) - 2), $key)
	If $listnum = 0 Then
		If $GUI_hotkeys[0] <> "no hotkey set" Then
			HotKeySet($GUI_hotkeys[0], "startbot")
			If StringLeft($GUI_hotkeys[0], 1) <> "+" Then
				HotKeySet("+" & $GUI_hotkeys[0], "startbot")
			EndIf
		EndIf
	EndIf
	If $listnum = 1 Then
		If $GUI_hotkeys[1] <> "no hotkey set" Then
			HotKeySet($GUI_hotkeys[1], "stopbot")
			If StringLeft($GUI_hotkeys[1], 1) <> "+" Then
				HotKeySet("+" & $GUI_hotkeys[1], "stopbot")
			EndIf
		EndIf
	EndIf
;~ 		if $listnum = 2 Then
;~ 			HotKeySet($GUI_hotkeys[$listnum],"bringupGUI")
;~ 		EndIf
EndFunc   ;==>setguihotkey1

Func setgui_hotkeys()
	For $i = 0 To 7
		If $macro_hotkeys[$i] <> "no hotkey set" Then
			HotKeySet($macro_hotkeys[$i], "macro_hotkeys" & $i)
		EndIf
	Next
EndFunc   ;==>setgui_hotkeys

Func disableguihotkey1()
	Local $listnum = StringLeft(GUICtrlRead($hotkeyGUIlist), 1)
	If $GUI_hotkeys[$listnum] <> "no hotkey set" Then
		HotKeySet($GUI_hotkeys[$listnum])
		If StringLeft($GUI_hotkeys[$listnum], 1) <> "+" Then
			HotKeySet("+" & $GUI_hotkeys[$listnum])
		EndIf
		$GUI_hotkeys[$listnum] = "no hotkey set"
		GUICtrlSetData($guihotkey, "no hotkey set")
		IniWrite($sIni, "GUI hotkeys", StringRight(GUICtrlRead($hotkeyGUIlist), StringLen(GUICtrlRead($hotkeyGUIlist)) - 2), "no hotkey set")
	Else
		MsgBox(0, "Error", "No hotkey can be removed if none is set.")
	EndIf
EndFunc   ;==>disableguihotkey1

#EndRegion Hotkey Functions
#EndRegion GUI


;~ HotKeySet("{Insert}", "getcoord")

$programstart = 0
;==============================================================================================================================================================
;==============================================================================================================================================================
;==============================================================================================================================================================
;==============================================================================================================================================================
Call("checkengaged")
;~ $count = 0
$loop = 0

While $loop = 0
	If $programstart = 1 Then
		WinActivate($title)
		WinWaitActive($title)
;~ 		WinActivate("c")
;~ 		WinWaitActive("$title")
		resetengage()
;~ 		If $checktype = 2 Then
;~ 			Call("checkandheal")
;~ 		EndIf
		Call("enemycoloursearchscript1")
		Call("scanarea", $enemycoord[0], $enemycoord[1])		; belum cek engage di sini, makanya kondisinya gini
		Call("checkxpgain")
		Sleep(50)
		ControlSend($title_b, "", $handle, "^a")
	EndIf
WEnd
;==============================================================================================================================================================
;==============================================================================================================================================================
;==============================================================================================================================================================
;==============================================================================================================================================================

Func Quit()
;~ 	If WinExists($title_b) Then WinSetTitle($title_b, "", $title)
	Exit
EndFunc   ;==>Quit

;===============================================||Ad lib function
Func ad_lib()
	If $flee_macro1 = 1 Then
		$flee_countercheck = $flee_countercheck + 1
		If $flee_countercheck >= $flee_checktime1 / 200 Then
			flee_macroplay()
			$flee_countercheck = 0
		EndIf
	EndIf
EndFunc   ;==>ad_lib
;===============================================||

#Region Heal functions
Func checkandheal()
	If $playback = 0 Then
		Readhealth()
		If $health / $healthmax * 100 < $flee_hlthlvl1 Then
			Call("checkengaged")
			If $engaged = 1 Then
				Call("enemycoloursearchscript1")
			EndIf
		EndIf
	EndIf
EndFunc   ;==>checkandheal

Func flee_macroplay()
	Readhealth()
	If $health / $healthmax * 100 <= $flee_hlthlvl1 Then ;$healthlvlflee Then
		macro_player($macro[0])
		ControlSend($title_b, "", $handle, "{ins}")
		Sleep(200)
		$stand = 0
		Local $h = 0
		While $h = 0
			Readhealth()
			If $health = $healthmax Then
				$h = 1
				ControlSend($title_b, "", $handle, "{ins}")
				Sleep(250)
				$stand = 1
			EndIf
			Sleep(100)
		WEnd
		macro_player($macro[1])
	EndIf
EndFunc   ;==>flee_macroplay
#EndRegion Heal functions

Func enemycoloursearchscript1()
	Local $enemyspotted = 0
	Call("checkengaged")
	If $engaged = 1 Then
		$enemyspotted = 1 ; don't enter loop if already engaged
	EndIf
	While $enemyspotted = 0
		ControlSend($title_b, "", $handle, "{shiftup}")
		ControlSend($title_b, "", $handle, "{shiftdown}")
		Sleep(5)
		For $iSwitch = 1 To 3 ;repeats until one is found or each is checked
			Switch $iSwitch
				Case 1
					$enemycoord = PixelSearch($searchxmin, $searchymin, $searchxmax, $searchymax, $enemylvllower, 0, 5)
					If Not @error Then
						$enemyspotted = 1
						$iSwitch = 3
					EndIf
				Case 2
					$enemycoord = PixelSearch($searchxmin, $searchymin, $searchxmax, $searchymax, $enemylvllow, 0, 5)
					If Not @error Then
						$enemyspotted = 1
						$iSwitch = 3
					EndIf
				Case 3
					$enemycoord = PixelSearch($searchxmin, $searchymin, $searchxmax, $searchymax, $enemylvlverylow, 0, 5)
					If Not @error Then
						$enemyspotted = 1
						$iSwitch = 3
					EndIf
			EndSwitch
		Next
;~ 		If $auto_sit1 = 1 And $stand = 1 Then		;autosit function.... should move it somewhere and organize it
;~ 				Sleep(50)
;~ 				ControlSend($title_b, "", $handle, "{shiftup}")
;~ 				ControlSend($title_b, "", $handle, "{ins}")
;~ 				ControlSend($title_b, "", $handle, "^a")
;~ 				ControlSend($title_b, "", $handle, "{shiftdown}")
;~ 				$stand = 0
;~ 		EndIf
		#cs
			If $enemyspotted = 1 And $programstart = 1 Then			;this is pretty useless right now... at least until I figure out how to determine if he is sitting or standing
			If $stand = 0 Then									;there is a way to make sure is he standing... use an action!!! bagus sekali :)
			ControlSend($title_b, "", $handle, "{shiftup}")
			ControlSend($title_b, "", $handle, "{ins}")
			ControlSend($title_b, "", $handle, "{shiftdown}")
			$stand = 1
			EndIf
			Sleep(50)
			If $enemycoord[0] > $maxwindowx - 100 Then $enemycoord[0] = $maxwindowx - 100		;useless junk as well... needs to be more exact... later
			If $enemycoord[0] < 10 Then $enemycoord[0] = 10
			If $enemycoord[1] < 10 Then $enemycoord[1] = 10
			If $enemycoord[1] > $maxwindowy - 90 Then $enemycoord[1] = $maxwindowy - 90
			Call("scanarea", $enemycoord[0], $enemycoord[1])
			Call("checkengaged")
			If $engaged = 0 Then
			ControlSend($title_b, "", $handle, "{shiftdown}")
			$enemyspotted = 0
			Else
			ControlSend($title_b, "", $handle, "{shiftup}")
			EndIf
			EndIf
		#ce
	WEnd
EndFunc   ;==>enemycoloursearchscript1

Func mousesearch()

EndFunc   ;==>mousesearch

Func enemycoloursearchscript_old()
	Global $enemycoord
	ControlSend($title_b, "", $handle, "{shiftdown}")
	Local $if = 0
	Local $s = 0
	Call("checkengaged")
;~ 	MsgBox(0,"tag","checkpoint 934")
	If $engaged = 1 Then
;~ 		MsgBox(0, "engaged", "you are engaged, loop should be skipped")
		$s = 1
;~ 		MsgBox(0, "engaged", "you are engaged, loop should be skipped")
	EndIf
	While $s = 0
;~ 		MsgBox(0,"tag","checkpoint 940, in loop")
		ControlSend($title_b, "", $handle, "{shiftup}")
		ControlSend($title_b, "", $handle, "{shiftdown}")
;~ 		Send("{shiftup}")
;~ 		Send("{shiftdown}")
;~ 	While $engaged = 0
;~ 	While checkengaged() = 0
		Sleep(5)
		$enemycoord = PixelSearch($searchxmin, $searchymin, $searchxmax, $searchymax, $enemylvllower, 0, 5)
		If Not @error Then
			$s = 1
			$if = 1
		EndIf
		If $if = 0 Then
			$enemycoord = PixelSearch($searchxmin, $searchymin, $searchxmax, $searchymax, $enemylvllow, 0, 5)
			If Not @error Then
				$s = 1
				$if = 1
			EndIf
		EndIf
		If $if = 0 Then
			$enemycoord = PixelSearch($searchxmin, $searchymin, $searchxmax, $searchymax, $enemylvlverylow, 0, 5)
			If Not @error Then
				$s = 1
				$if = 1
			EndIf
		EndIf
;~ 		MsgBox(0,"tag","checkpoint 959")
		If $auto_sit1 = 1 Then
			If $if = 0 And $stand = 1 Then
;~ 				Sleep(50)
				ControlSend($title_b, "", $handle, "{shiftup}")
				ControlSend($title_b, "", $handle, "{ins}")
;~ 				ControlSend($title_b, "", $handle, "^a")
				ControlSend($title_b, "", $handle, "{shiftdown}")
				$stand = 0
			EndIf
		EndIf
;~ 		If $engaged = 1 And $programstart = 1 Then
		If $s = 1 And $programstart = 1 Then
			If $stand = 0 Then
				ControlSend($title_b, "", $handle, "{shiftup}")
				ControlSend($title_b, "", $handle, "{ins}")
;~ 				Sleep(50)
				ControlSend($title_b, "", $handle, "{shiftdown}")
				$stand = 1
			EndIf
;~ 			msgbox(0,"search is finding something still","coord[0] and coord[1] are "&$enemycoord[0]&" "&$enemycoord[1])
			Sleep(50)
;~ 			Send("{f3}")
;~ 			Send("{f3}")
			Sleep(50)
			If $enemycoord[0] > $maxwindowx - 100 Then $enemycoord[0] = $maxwindowx - 100
			If $enemycoord[0] < 10 Then $enemycoord[0] = 10
			If $enemycoord[1] < 10 Then $enemycoord[1] = 10
			If $enemycoord[1] > $maxwindowy - 90 Then $enemycoord[1] = $maxwindowy - 90
;~ 			MouseClick("left", $enemycoord[0] + 50, $enemycoord[1] + 40, 1, 0)
			Call("scanarea", $enemycoord[0], $enemycoord[1])
			Call("checkengaged")
			If $engaged = 0 Then
				ControlSend($title_b, "", $handle, "{shiftdown}")
				$s = 0
				$if = 0
			Else
				ControlSend($title_b, "", $handle, "{shiftup}")
			EndIf
		EndIf
;~ 		Readlast() ;changed
;~ 		If $lastaction = $currentweapon Then $s = 1 ;changed
;~ 		If $lastaction = $currentweapon Then $engaged = 1
	WEnd
EndFunc   ;==>enemycoloursearchscript_old

Func scanarea($x, $y)
	Select
		Case $defaultattackfull = 1
			For $i = 0 To 8
;~ 		MouseMove($x + ($i * 4 * 3), $y + $i * 3 * 3, 0)
				MouseMove($x + $i * 4 * 1.5, $y + $i * 4 * 1.5 + 35, 0)
				Readcursorid()
				If $cursorid = 7 Then
					MouseClick("left")
;~ 				MouseClick("left",$x + $i * 4 * 1.5, $y + $i * 4 * 1.5 + 35,2)
					ExitLoop
				EndIf
				Readcursorid()
				MouseMove($x - $i * 4 * 1.5, $y + $i * 4 * 1.5 + 35, 0)
;~ 		MouseMove($x - ($i * 4 * 3), $y + $i * 3 * 3, 0)
				If $cursorid = 7 Then
					MouseClick("left")
;~ 				MouseClick("left",$x + $i * 4 * 1.5, $y + $i * 4 * 1.5 + 35,2)
					ExitLoop
				EndIf
			Next

		Case $defaultattackclose = 1
			For $i = 0 To 7
				MouseMove($x + $i * 4 * 2, $y + $i * 4 * 1.5 + 35, 0)
				Readcursorid()
				If $cursorid = 7 Then
;~ 				ControlClick($title_b,"","","left",1)
					MouseClick("left")
					ExitLoop
				EndIf
			Next
	EndSelect
EndFunc   ;==>scanarea

Func checkengaged()
	Sleep(200)
	Readlast()
	If $lastaction = $currentweapon Then
		Return $engaged = 1
;~ 		$engaged = 1
	Else
		Return $engaged = 0
;~ 		$engaged = 0
	EndIf
EndFunc   ;==>checkengaged

Func resetengage()
	$M_open = _MemoryOpen($Process)
;~ 	msgbox(0,"value of $M_open is", $M_open[0])
	_MemoryWrite($M_open, "0x" & $mem_lastaction, 1869768026)
	_MemoryClose($M_open)
EndFunc   ;==>resetengage

Func checkxpgain2()
	Local $c = 0
	Local $w = 0
	While $w = 0
		Readlast()
		If $lastaction = $xpgain Then $w = 1
		$c = $c + 1
		If $c > $failsafetime / 50 Then
			$w = 1
			resetengage()
		EndIf
		Sleep(50)
	WEnd
EndFunc   ;==>checkxpgain

Func checkxpgain()
	Local $c = 0
	Local $w = 0
	While $w = 0
		Readlast()
		If $lastaction = $xpgain Then $w = 1
		$c = $c + 1
		If $c > $failsafetime / 50 Then
			$w = 1
			resetengage()
		EndIf
		Sleep(50)
	WEnd
EndFunc   ;==>checkxpgain

Func standcheck()
	WinActivate("Pirate King")
	WinWaitActive("Pirate King")
	If $maxwindowx = 1024 Then
		MouseMove(512, 384, 0)
		Opt("mouseclickdowndelay", 2000)
		MouseClick("left")
		Opt("mouseclickdowndelay", 10)
		MouseClick("left")
		MouseClick("left")
	EndIf
	If $maxwindowx = 800 Then
		MouseMove(400, 300, 0)
		Opt("mouseclickdowndelay", 2000)
		MouseClick("left")
		Opt("mouseclickdowndelay", 10)
		MouseClick("left")
		MouseClick("left")
	EndIf
EndFunc   ;==>standcheck

#Region read/write memory
Func _MemoryOpen($iv_Pid, $iv_DesiredAccess = 0x1F0FFF, $if_InheritHandle = 1)

	If Not ProcessExists($iv_Pid) Then
		MsgBox(0, "value of lastaction", "error3")
		SetError(1)
		Return 0
	EndIf

	Local $ah_Handle[2] = [DllOpen('kernel32.dll')]
;~ 	Local $ah_Handle = DllOpen('kernel32.dll') 	-the brackets are because an array is being declared, no brackets neccessary if it was a standalone variable
;~ msgbox(0,"value of ah_Handle[2] is", $ah_Handle[1])
	If @error Then
		MsgBox(0, "value of lastaction", "error4")
		SetError(2)
		Return 0
	EndIf
;~ msgbox(0,"value of ah_Handle[0] is", $ah_Handle[0]&"inherithandle"&$if_InheritHandle&"and the iv_pid"&$iv_Pid)
	Local $av_OpenProcess = DllCall($ah_Handle[0], 'int', 'OpenProcess', 'int', $iv_DesiredAccess, 'int', $if_InheritHandle, 'int', $iv_Pid)
;~ 	Local $av_OpenProcess = DllCall('kernel32.dll', 'int', 'OpenProcess', 'int', 0x1F0FFF(all access), 'int', 1, 'int', 4084)
;~ msgbox(0,"value of $av_openprocess is", $av_OpenProcess[0]) ;1820
;~ msgbox(0,"value of iv_pid is", $iv_Pid) ; 4084
	If @error Then
		MsgBox(0, "value of lastaction", "error5")
		DllClose($ah_Handle[0])
		SetError(3)
		Return 0
	EndIf

	$ah_Handle[1] = $av_OpenProcess[0]
;~ msgbox(0,"value of ah_Handle is", $ah_Handle)
	Return $ah_Handle

EndFunc   ;==>_MemoryOpen


Func _MemoryRead($ah_Handle, $iv_Address, $sv_Type = 'dword') ; dword is 4 bytes

	If Not IsArray($ah_Handle) Then
		SetError(1)
		MsgBox(0, "value of lastaction", "error1")
		Return 0
	EndIf

	Local $v_Buffer = DllStructCreate($sv_Type) ;dword - maybe later I will try changing to 'char[4]' so that it will be easier to program

	If @error Then
		MsgBox(0, "value of lastaction", "error2")
		SetError(@error + 1)
		Return 0
	EndIf

;~ 	DllCall($ah_Handle[0], 'int', 'ReadProcessMemory', 'int', $iv_Address, 'int', $ah_Handle[1], 'int', DllStructGetSize($v_Buffer), 'int', '', 'ptr', DllStructGetPtr($v_Buffer))
	DllCall($ah_Handle[0], 'int', 'ReadProcessMemory', 'int', $ah_Handle[1], 'int', $iv_Address, 'ptr', DllStructGetPtr($v_Buffer), 'int', DllStructGetSize($v_Buffer), 'int', '')
;~ msgbox(0,"value of ah_Handle[0],[1] is", $ah_Handle[0]&", "&$ah_Handle[1]);1,1788
	If Not @error Then
		Local $v_Value = DllStructGetData($v_Buffer, 1)
		Return $v_Value
	Else
		SetError(6)
		Return 0
	EndIf

EndFunc   ;==>_MemoryRead


Func _MemoryWrite($ah_Handle, $iv_Address, $v_Data, $sv_Type = 'dword')

	If Not IsArray($ah_Handle) Then
		SetError(1)
		Return 0
	EndIf

	Local $v_Buffer = DllStructCreate($sv_Type)

	If @error Then
		SetError(@error + 1)
		Return 0
	Else
		DllStructSetData($v_Buffer, 1, $v_Data)
		If @error Then
			SetError(6)
			Return 0
		EndIf
	EndIf

	DllCall($ah_Handle[0], 'int', 'WriteProcessMemory', 'int', $ah_Handle[1], 'int', $iv_Address, 'ptr', DllStructGetPtr($v_Buffer), 'int', DllStructGetSize($v_Buffer), 'int', '')

	If Not @error Then
		Return 1
	Else
		SetError(7)
		Return 0
	EndIf

EndFunc   ;==>_MemoryWrite


Func _MemoryClose($ah_Handle)

	If Not IsArray($ah_Handle) Then
		SetError(1)
		Return 0
	EndIf

	DllCall($ah_Handle[0], 'int', 'CloseHandle', 'int', $ah_Handle[1])
	If Not @error Then
		DllClose($ah_Handle[0])
		Return 1
	Else
		DllClose($ah_Handle[0])
		SetError(2)
		Return 0
	EndIf

EndFunc   ;==>_MemoryClose

Func Readlast()
	$M_open = _MemoryOpen($Process)
	$lastaction = _MemoryRead($M_open, "0x" & $mem_lastaction)
	_MemoryClose($M_open)
EndFunc   ;==>Readlast

Func Readcursorid()
	$M_open = _MemoryOpen($Process)
	$cursorid = _MemoryRead($M_open, "0x" & $mem_cursorid, 'byte')
	_MemoryClose($M_open)
EndFunc   ;==>Readcursorid

Func Readhealth()
	$M_open = _MemoryOpen($Process)
	$health = _MemoryRead($M_open, "0x" & $mem_health)
	$healthmax = _MemoryRead($M_open, "0x" & $mem_healthmax)
	_MemoryClose($M_open)
EndFunc   ;==>Readhealth

Func Readscreenresolution()
	$M_open = _MemoryOpen($Process)
	$screenresolution = _MemoryRead($M_open, "0x" & $mem_screenresolution)
	_MemoryClose($M_open)
EndFunc   ;==>Readscreenresolution

Func Readpointers()
	$M_open = _MemoryOpen($Process)
	$mem_health = String(Hex(("0x" & $offsethealth) + _MemoryRead($M_open, "0x" & $mem_pointerhealth)))
	$mem_healthmax = String(Hex(("0x" & $offsethealthmax) + _MemoryRead($M_open, "0x" & $mem_pointerhealth)))
	$mem_experience = String(Hex(("0x" & $offsetexperience) + _MemoryRead($M_open, "0x" & $mem_pointerexperience)))
	_MemoryClose($M_open)
EndFunc   ;==>Readpointers

Func Readweapon()
	$M_open = _MemoryOpen($Process)
	Local $weapon = _MemoryRead($M_open, "0x" & $mem_weapon, 'byte')
	_MemoryClose($M_open)
	Switch $weapon
		Case 185
			$currentweapon = $Dual_Weapon

		Case 141
			$currentweapon = $Greatsword

		Case 146
			$currentweapon = $Bow

		Case 151
			$currentweapon = $Firegun

		Case 137
			$currentweapon = $Sword

		Case 156 ;guessing aja
			$currentweapon = $Dagger

		Case 161 ;guessing aja
			$currentweapon = $Short_Staff

		Case 122
			$currentweapon = $Bare_Hand
	EndSwitch
EndFunc   ;==>Readweapon

#cs Corresponding Weapon Values
	Global $Dual_Weapon = 1818326340 	;185 *values for new way to find equipped weapon
	Global $Greatsword = 1634038343 	;141
	Global $Sword = 1919907667 			;137
	Global $Bow = 7827266 				;146
	Global $Firegun = 1701996870 		;151
	Global $Dagger = 1734828356
	Global $Short_Staff = 1919903827
	Global $Bare_Hand = 1701994818 		;122
#ce

;~ Func Readstand()
;~ 	$M_open = _MemoryOpen($Process)
;~ 	$stand = _MemoryRead($M_open, "0x" & $mem_sitvalue)
;~ 	$stand = $stand - 62
;~ 	_MemoryClose($M_open)
;~ EndFunc   ;==>Readstand
#cs
	Func Score_p1_Write()
	If $Score_p1_freeze_c1 = 1 Then
	$Score_p1_Read = $freeze_score_p1
	$Score_p1_freeze_c1 = 0
	ConsoleWrite($freeze_score_p1 & @CR)
	Else
	$Score_p1_Read = GUICtrlRead($Input_p1_Score_edit)
	EndIf
	
	$M_open = _MemoryOpen($Process)
	;~ 	msgbox(0,"value of $M_open is", $M_open[0])
	_MemoryWrite($M_open, "0x" & $Mem_Score_p1, "0x" & Hex($Score_p1_Read))
	_MemoryClose($M_open)
	$freeze_score_p1 = $Score_p1_Read
	EndFunc   ;==>Score_p1_Write
#ce
#EndRegion read/write memory