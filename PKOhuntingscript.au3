#Include <Misc.au3>
AutoItSetOption("pixelcoordmode",2)
AutoItSetOption("mousecoordmode",2)
winactivate("Pirate")
winwaitactive("Pirate")

$timemodifier=200
;~ $xmultiplier=25
;~ $ymultiplier=19
$maxrange=18

;x compensating variables
$xmaxmultiplier=28
$xminmultiplier=22  ;was 18 but that's not effective

;y compensating variables
$ymaxminimap=18
$yminminimap=-13
$ymaxmultiplier=20
$yminmultiplier=14





;0xFFB400 - colour of metoerites on minimap
;0xFF0000 - colour of enemies on minimap

$e=0
$l=0
$a=0
	while $l=0
		$enemysrch=PixelSearch(730-$a,72-$a,730+$a,72+$a,0xFF0000) ;note this colour is center of enemy icon on map :) to be precautious you can add a shade variable but I don't think it's nessecary
		if not @error Then
;~ 			MsgBox(0,"variables","$a="&$a&". $enemysrch[0]="&$enemysrch[0]&" $enemysrch[1]="&$enemysrch[1])
;~ 			mousemove($enemysrch[0]+3,$enemysrch[1]+29,3) 
			$l=1					; build a cartesian plane for ease of number use
			$kx=($enemysrch[0]-730)	; this is making the coord found relative to the x location center on the mini, will return pos or neg value, like a cartesian plane.
			$ky=(72-$enemysrch[1])	; this makes the 7 coord found relative to the y location center on the mini map.
;~ 			MsgBox(0,"x and y value relative to center","x="&$kx&" y="&$ky)
			$distance=(($kx^2+$ky^2)^(1/2))
			$timeout=$distance*$timemodifier ; note, $kx and $ky are now the relation between $enemysrch and the coord 730,72 , which was found to be the center of the arrow on the minimap.
			$xmultiplier=$xminmultiplier + (-($ky-$ymaxminimap)*(($xmaxmultiplier-$xminmultiplier)/($ymaxminimap-$yminminimap)))
			$ymultiplier=$yminmultiplier + (-($ky-$ymaxminimap)*(($ymaxmultiplier-$yminmultiplier)/($ymaxminimap-$yminminimap)))
;~ 			MsgBox(0,"x and y multipliers are..","xmult="&$xmultiplier&" ymult="&$ymultiplier)
			$kx=$kx*$xmultiplier
			$ky=$ky*$ymultiplier
 				; = (a^2 +b^2)^(1/2)
;~ 			MsgBox(0,"variables2","$kx="&$kx&" $ky="&$ky&". $timeout="&$timedistanceout)
			Mousemove(400+$kx,270+$ky,2)
;~ 			msgbox(0,"here","resulting location is here")
			if $kx<-400 then $kx=-395
			if $kx>400 then $kx=398
			if $ky<-270 then $ky=-268
			if $ky>280 then $ky=280
			Send("!m")
			MouseClick("left",400+$kx,270-$ky,1,0)
			Send("!m")
			sleep(10)
			PixelSearch(180,10,637,550,0xFF9CAC,2)   ;search for red thing over enemy head which appears when engaged/engaging
		if not @error Then
			MouseClick("left",400+$kx,270-$ky,1,0)
		EndIf
			sleep($timeout)					;give him enough time to get to the spot on the map according to the distance he is moving
;~ 			msgbox(0,"time","times up with a modifier of "&$timemodifier&" and a resulting time of "&$timeout)
		EndIf
		$a=$a+1
		if $a>=$maxrange then $l=1
	WEnd	

; formula...  14+(-(n-15)(5/28)) 
; $minmultiplier + (-($ky-$maxYminimap)(($maxmultiplier-$minmultiplier)/($maxYminimap-$minYminimap))