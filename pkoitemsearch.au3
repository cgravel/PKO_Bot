

Opt("pixelcoordmode", 2)
Opt("mousecoordmode", 2)
Opt("GUIOnEventMode", 1)
Opt("WinTitleMatchMode", 4)
Opt("MouseClickDelay", 20)
Opt("MouseClickDownDelay", 30)

;~ $check =
Global $msg = @CRLF&"1 -> Short Sword" & @CRLF & _
		"2 -> Long Sword" & @CRLF & _
		"3 -> Fencing Sword" & @CRLF & _
		"4 -> Serpentine Sword" & @CRLF & _
		"5 -> Dazzling Sword" & @CRLF & _
		"6 -> Wyrm Sword" & @CRLF & _
		"7 -> Sacro Sword" & @CRLF & _
		"8 -> Newbie Knife" & @CRLF & _
		"9 -> Newbie Sword" & @CRLF & _
		"10 -> Short Metal Sword"
;~ 11 -> Steel Sword_
;~ 12 -> Striking Sword_
;~ 13 -> Two Handed Sword_
;~ 14 -> Warrior Sword_
;~ 15 -> Criss Sword_
;~ 16 -> Paladin Sword_
;~ 17 -> Bone Sword_
;~ 18 -> Thunder Blade_
;~ 19 -> Onyx Sword_
;~ 20 -> Rebel Sword_
;~ 21 -> Charging Sword_
;~ 22 -> Crescent Sword_
;~ 23 -> Delusion Sword_
;~ 24 -> Chest of Cabal_
;~ 25 -> Short Bow_
;~ 26 -> Long Bow_
;~ 27 -> Tribal Bow_
;~ 28 -> Meteor Bow_
;~ 29 -> Recca Bow_
;~ 30 -> Bow of Dawn")
;~ MsgBox(0, "", $msg)
;~ $i = 2
for $i =1 to 9
;~ $match=StringRegExp($msg,"(?<="&$i&")(?: -> )(.*)(?=(?m))",1)
;~ $match=StringRegExp($msg,"(?:\v)(?:[\d]{1,4} -> )(.*)",1)
$match=StringRegExp($msg,"(?:\v)(?:"&$i&" -> )(.*)",1) 
; (?:\v) to find a vertical whitespace before and not include
; (?:$i -> ) this is to find a specific number
;~ MsgBox(0,"",@error)
MsgBox(0,"",$match[0])
Next
;~ 	MsgBox(64, "Help on Recorder Function", _
;~ 			"What it's used for:" & @CRLF & _
;~ 			"This function is used to get the character out of danger when he has low health" & @CRLF & _
;~ 			"currently this function will only execute after he has killed an enemy so don't" & @CRLF & _
;~ 			"expect him to run while engaged in a fight. This just sets the course for the " & @CRLF & _
;~ 			"character to run to when he's low on health so he can sit and heal.")